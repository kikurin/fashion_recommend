# FoveaBox
- anchor-free framework for object detection
- https://github.com/taokong/FoveaBox


<img src='https://g.gravizo.com/svg?
digraph exempleSO {
    rankdir="DT";
    node [style="", shape=box]
    h5[width =0.8, height=0.2];
    h4[width=1.0, height = 0.2];
    h3[width=1.2, height = 0.2];
    h2[width=1.4, height = 0.2];
    h1[width=1.6, height = 0.2];
    p7[width =0.8, height=0.2];
    p6[width=1.0, height = 0.2];
    p5[width=1.2, height = 0.2, label="p5 = conv(.)"];
    p4[width=1.4, height = 0.2, label="p4=conv(.) \n .= conv(h4) + up(p5)"];
    p3[width=1.6, height = 0.2, label="p3=conv(.)"];
    h5 -> h4[arrowhead="none"]
    h4 -> h3[arrowhead="none"]
    h3 -> h2[arrowhead="none"]
    h2 -> h1[arrowhead="none"]
    {rank=same; h3, p3}
    {rank=same; h4, p4 }
    {rank=same; h5, p5 }
    p5 -> p4[label="upsample(p5)"]
    p4 -> p3[label="upsample(p4)"]
    h3->p3[label = "conv(h3)"]
    h4 -> p4[label = "conv(h4)"]
    h5 ->p5[label = "conv(h5)"]
    p7->p6[arrowhead="none", label="⬆ conv(p6, stride=2)"]
    p6->p5[arrowhead="none", label="⬆ conv(p5, stride=2)"]]
}
'/>

<br><br><br>

<img src='https://g.gravizo.com/svg?
digraph exempleSO {
    rankdir="LR";
    node [style="", shape=box]
    pyramid_layer[width =1.6, height=0.2]
    class[label="class (W, H, K)"]
    box[label="class (W, H, 4)"]
    pyramid_layer -> class_subnet
    pyramid_layer -> box_subnet
    class_subnet -> class
    box_subnet -> box
}
'/>


<br>


## Feature Pyramid Network Backbone
- l = 3, 4, ..., 7
- $P_l$ has $(1/2)^l$ resolution of the input
    - feature0 --> feature1 --> feature...7
    - p3 <-- p4 <---................<-  p7
- all Pyramid levels have C = 256

## Scale Assignment
- for l = 3, 4, ..., 7, the basic area are $32^2, ...., 512^2$
- basic area $S_l = 4^l S_0$
- set $S_0=16$
    - $S_3 = 4^3 * 16 = 1024 = 32*32$
    - ...
    - $S_7 = 4^7 * 16 = 1024 = 512 *512$
- each feature pyramid learns to be responsive to objects of particular scales. The valid scale range of target boxes for pyramid level l :
    - $[s_l/\eta^2, s_l \eta^2]$
    - $\eta$ is set empirically to control the scale
        - $\eta = \sqrt 2$:no overlaps

|l|$\eta=\sqrt 2$| $\eta = 2$|
|---|---|---|
|3| [512, 2048] | [256, 4096]
|4| [2048, 8192] |[1024, 16384]

- Target objects not in the corresponding scale range are ignored.
- Same object can be detected by multiple pyramids


## Object Fovea
- Map the ground-truth box into the target feature pyramid $P_l$ with stride $2_l$
    - $x_1' = \frac{x_1}{2^l}$
    - $x_2' = \frac{x_2}{2^l}$   
    - $y_1' = \frac{y_1}{2^l}$
    - $y_2' = \frac{y_2}{2^l}$
    - $c_x' = x_1' + 0.5(x_2' - x_1')$, $c_y' = y_1' + 0.5(y_2' - y_1')$

<br>
<img src='https://g.gravizo.com/svg?
digraph exempleSO {
    rankdir="LR";
    graph
    {
        label="\G"
        subgraph "cluster_all"
        {
            subgraph "cluster_neg"
            {
               cluster_pos[shape=box];
            }
        }
    }
}
'/>
<br>

- Positive area (fovea) $R^{pos} = (x_1'', y_1'', x_2'', y_2'')$
    - $x_1'' = c_x' -  0.5*(x_2' - x_1')\sigma_1$
    - $y_1'' = c_y' -  0.5*(y_2' - y_1')\sigma_1$
    - $x_2'' = c_x' + 0.5*(x_2' - x_1')\sigma_1$
    - $y_2'' = c_y' +  0.5*(y_2' - y_1')\sigma_1$
    - where $\sigma_1$ is the shrink factor
    - Each cell inside the positive area is annotated with the corresponding target class label
- Negative samples is the same, $R^{neg}$ but using $\sigma_2$
    - $\sigma_2 > \sigma_1$
    -  Area between $R^{neg}, R^{pos}$  is neutral
    - all_area - $R^{neg}$ as negative area, and $R^{pos}$ as positive area
- loss: focal loss

<br><br><br><br>

## Box prediction
- all feature cells (x, y) inside $R^neg$ is annotated with target box prediction
    - if multiple boxes are inside the same cell, keep the smallest box
-  network localization outputs $(t_{x_1}, t_{y_1}, t_{x_2}, t_{y_2})$ at cell (x, y) in the feature maps
    - $t_{x_1} = log \frac{2^l(x+ 0.5)-x_1}{z}$
    - $t_{y_1} = log \frac{2^l(y+ 0.5)-y_1}{z} \\$
    - $t_{x_2} = log \frac{x_2 - 2^l(x+ 0.5)}{z}$
    - $t_{y_2} = log \frac{y_2 - 2^l(y+ 0.5)}{z}$
    - $z = \sqrt s_l$, normalization factor to project the output space to space centered around 1
    - when l = 3, feature map is [32 *32] so the z = 32
        - t: log(offset)
- loss: smooth l1



<br><br><br><br>


## Focal loss
- cross entropy loss $L_{ce} = -y log \sigma(x) - (1-y)log(1-\sigma(x))$
    - if y =1, loss = $-log  \sigma(x)$
        - $\sigma(x) $ close to 0, then loss is close to -(-inf) = inf
    - if y= 0, loss = $-log (1-\sigma(x))$
        - $\sigma(x)$ between 1, then loss is close to inf

- $p_t = p, \alpha_t = \alpha$, if y=1
- $p_t = 1- p, \alpha_t = 1-\alpha_t$, if y = 0
- $L_{ce} = -log(p_t)$
- $L_{fl} = -\alpha_t(1-p_t)^\gamma log(p_t)$
    - weight: $\alpha_t(1-p_t)^\gamma$
    - when $p_t$ gets closer to closer to its labels (y), the weight decelerates. (lower weight for good-classified examples)
    - when $\alpha > 0.5$, larger weight for positive examples




<br><br><br><br>

## ROIAlign
- feature_map size: conv_h, conv_w
- pool_size; pool_h, pool_w
- bbox: showing the region of interest
- sampling ratio: number of points sampled in grid-cell
- (1) Get region of interest in feature map by bbox
- (2) Bilinear sampling to get unpooled feature map [pool_h * sampling_ratio, pool_w * sampling_ratio]
- (3) Average pool get [pool_h , pool_w]



<br><br><br><br>

## Pose Estimation
- K keypoint types
- Network --> predict K masks [m*m] (only 1 ground-truth point in each mask)
- minimize cross-entropy loss over $m^2$ softmax output



<br><br><br><br>
