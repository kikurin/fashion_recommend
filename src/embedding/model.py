import tensorflow as tf
import numpy as np
from tqdm import trange

from mrcnn.loss import Loss
from mrcnn import model as M
from embedding.net import Embedding
from embedding.dataloader import DataLoader, TrainDataLoader
from yolact.evaluation import evaluate_mAP, arrange_single_mask_data
from fovea.model import poly_decay, flat

class ModelBase():
    def __init__(self, config, img_size, n_class, n_sample, n_feature, nms_max_n, **net_args):    
        self.config = config
        self.img_size = H, W = img_size
        self.n_sample = n_sample
        self.n_class = n_class
        self.n_nms = nms_max_n
        self.n_feature = n_feature
        self.graph = tf.Graph()    
        with self.graph.as_default():
            with tf.name_scope('network'):
                self.img = tf.placeholder(tf.float32, [None, None, H, W, 3])
                self.is_training = tf.placeholder(tf.bool, [])
                self.define_network(n_feature = n_feature, nms_max_n = nms_max_n, **net_args)
            self.saver = tf.train.Saver()    

    def define_network(self, **net_args):
        self.net = Embedding(n_class = self.n_class, **net_args)
        self.pred, self.pred_bbox, self.nms, self.nms_mask, self.nms_pred_mask_logit, \
            self.nms_pred_mask, self.nms_pred_class, self.nms_pred_logit, self.emb = self.net.forward(
                tf.reshape(self.img, [-1] + self.img_size + [3]), self.is_training)
        self.coder = self.net.coder   

    def define_target(self):
        with self.graph.as_default():
            with tf.name_scope('target'):
                self.gt_box = tf.placeholder(tf.float32, [None, 1+2*self.n_sample, None, 6])
                self.instance = tf.placeholder(tf.float32, [None, 1+2*self.n_sample] + self.img_size)
                self.reshape_instance = tf.reshape(self.instance, [-1] + self.img_size)
                self.target = self.coder.encode(tf.reshape(self.gt_box, [-1,tf.shape(self.gt_box)[2],  6]))
                self.true_bbox = self.coder.decode(self.target)
                self.target, self.true_bbox = flat(self.target), flat(self.true_bbox)

    def get_pred_mask_value(self, pred_mask, pred_class):
        return M.ModelBase.get_pred_mask_value(self, pred_mask, pred_class)

    def save_model(self, *args):
        M.ModelBase.save_model(self, *args)

    def restore_model(self, *args):
        M.ModelBase.restore_model(self, *args)

    def define_dataset(self, filename, **kwargs):
        with self.graph.as_default():
            self.dat = DataLoader(filename, self.img_size, self.n_sample, **kwargs)  


            

def nms_emb_distance(emb1, emb2, prob1, prob2):
    '''
    Args:
        - emb1/emb2: [batch_size, 1/n_sample, n_nms, n_feature]
        - prob1/prob2: [batch_size, 1/n_sample, n_nms, n_class]
    Returns:
        - dist: [batch_size, n_sample]
    
    '''
    distance_fn = lambda x, y: tf.sqrt(tf.reduce_sum((x-y)**2, -1))
    dist = distance_fn(emb1[:, :, :, None], emb2[:, :, None]) #[B, n_sample, n2, n1]
    cat1 = tf.argmax(prob1, -1)-tf.cast(tf.equal(tf.reduce_max(prob1, -1), 0), tf.int64)
    cat1 = cat1[:, :, None] #[B, 1, 1, n1]
    cat2 = tf.argmax(prob2, -1)-tf.cast(tf.equal(tf.reduce_max(prob2, -1), 0), tf.int64)
    cat2 = cat2[..., None] #[B, n_sample, n2, 1]
    cat_mask = tf.cast(tf.equal(cat1, cat2), tf.float32)  #[B, n_sample, n2, n1]
    dist1 = dist * tf.where(cat_mask>0, tf.ones_like(cat_mask), 100*tf.ones_like(cat_mask))
    
    dist1 = tf.reduce_min(dist1, [-1, -2]) #[B, n_smaple]
    return dist1
            
            
            
class ModelTrain(ModelBase):
    def __init__(self, base_args, data_args, loss_args, opt_args):
        ModelBase.__init__(self, **base_args)
        self.define_dataset(**data_args)
        self.define_loss(**loss_args)
        self.define_optimizer(**opt_args)
        
    def define_dataset(self, train_filename, valid_filename, **kwargs):
        with self.graph.as_default():
            self.train_dat = TrainDataLoader(train_filename, self.img_size, self.n_sample,  **kwargs)
            self.valid_dat = TrainDataLoader(valid_filename, self.img_size, self.n_sample, **kwargs)

    def define_loss(self, focal_alpha = 0.25, focal_gamma=2, smooth_l1_beta=0.11, iou_threshold=0.5, 
                    distance_margin = 10):
        with self.graph.as_default():
            self.define_target()
            loss_fn = Loss(focal_alpha, focal_gamma, smooth_l1_beta, iou_threshold,
                            self.net.obj_thresh, self.net.nms_thresh, self.net.nms_max_n)
            with tf.name_scope('loss'):
                self.loss_dict = loss_fn(self.target, self.pred, self.reshape_instance, self.nms_pred_mask_logit,
                                         self.true_bbox, self.pred_bbox, self.nms, self.nms_mask, self.nms_pred_logit)  

                emb = tf.reshape(self.emb, [-1, 1+2*self.n_sample, self.n_nms, self.n_feature])
                pred_class = tf.reshape(self.nms_pred_class, [-1, 1+2*self.n_sample, self.n_nms, self.n_class])
                emb_query, prob_query = emb[:, 0:1], pred_class[:, 0:1]
                emb_pos, prob_pos = emb[:, 1:1+self.n_sample], pred_class[:, 1:1+self.n_sample]
                emb_neg, prob_neg = emb[:, 1+self.n_sample:], pred_class[:, 1+self.n_sample:]

                dis_pos = nms_emb_distance(emb_query, emb_pos, prob_query, prob_pos)
                dis_neg = nms_emb_distance(emb_query, emb_neg, prob_query, prob_neg)
                loss_distance = tf.clip_by_value( tf.reduce_max(dis_pos, 1) - tf.reduce_min(dis_neg, 1) , -distance_margin, 100000)
                self.loss_dict['loss_distance'] = tf.reduce_mean(loss_distance)

    def define_optimizer(self, init_lr, lr_decay, alpha_l2, alpha_bbox, alpha_class, alpha_mask, alpha_distance, 
                    alpha_nms_class = None, momentum=0.9):
        with self.graph.as_default():    
            with tf.name_scope('loss'):
                loss = alpha_l2 * tf.reduce_mean([tf.nn.l2_loss(v) for v in tf.trainable_variables()])+ \
                        alpha_class * self.loss_dict['loss_class'] +\
                        alpha_bbox * self.loss_dict['loss_bbox'] +\
                        alpha_mask * self.loss_dict['loss_mask'] + \
                        alpha_distance * self.loss_dict['loss_distance']
                if alpha_nms_class is not None:
                    loss += alpha_nms_class * self.loss_dict['loss_nms_class']     
                self.loss_dict['loss'] = loss       
            with tf.name_scope('optimizer'):
                self.init_lr = init_lr
                self.lr_decay = lr_decay
                self.lr = tf.placeholder(tf.float32, [])
                opt = tf.train.MomentumOptimizer(self.lr, momentum)  
                v = tf.trainable_variables()
                g = tf.gradients(loss, v)
                #g = tf.clip_by_global_norm(g, 5)[0]
                opt = opt.apply_gradients(list(zip(g,v)))
                self.opt = tf.group([opt, tf.get_collection(tf.GraphKeys.UPDATE_OPS)])   
                
    def one_epoch(self, sess, dat, epoch_size, lr, train = False):
        summary = np.zeros(len(self.loss_dict.keys()))
        dats = []  
        sess_op = [self.loss_dict, self.nms_pred_mask, self.nms_pred_class]
        if train:
            sess_op += [self.opt]
        
        for _ in trange(epoch_size):
            try:
                img, instance, gt_box = sess.run(dat.next_op)
            except:
                dat.initialize(sess) 
                img, instance, gt_box = sess.run(dat.next_op)    
            feed_dict = {self.img: img, self.instance: instance, self.gt_box: gt_box, self.is_training: True,
                            self.lr: lr}
            sess_value= sess.run(sess_op, feed_dict)
            loss_dict = sess_value[0]
            summary += list(loss_dict.values()) 
            # mAP data
            gt_box = gt_box.reshape([-1] + list(gt_box.shape[2:]))
            instance = instance.reshape([-1] + self.img_size)
            pred_mask, pred_class = sess_value[1:3]            
            true_mask = [[(ins==b[5]) * (b[4]+1) for b in box if b[-1]>0] for box, ins in zip(gt_box, instance)]
            pred_mask = self.get_pred_mask_value(pred_mask, pred_class)            
            pred_obj = [[np.max(p) for p in p_class if p.sum()>0] for p_class in pred_class]
            dats += [arrange_single_mask_data(p, t, conf, self.n_class) for p, t, conf in zip(pred_mask, true_mask, pred_obj)]   

        summary /= epoch_size
        mAP= evaluate_mAP(dats, self.n_class, 0.5) 
        return summary.tolist() + [mAP]

    def train(self, **kwargs):
        M.ModelTrain.train(self, **kwargs)