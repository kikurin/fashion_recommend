import tensorflow as tf
import numpy as np
import json
import itertools
import math

from yolact import dataloader as D

class DataLoader():
    def __init__(self, filename, img_size, n_sample, batch_size, buffer_size, num_threads, 
                 n_max_box=0, data_aug = False, with_label = False, with_item = False, no_obj_sampling_w=None):
        # if no_obj_sampling_w=1, same weight as obj
        
        self.img_size = img_size
        self.data_aug = data_aug
        self.with_label = with_label
        self.n_max_box = n_max_box
        self.with_item = with_item
        self.n_sample = n_sample      

        try:
            json_data = json.load(open(filename)) 
            if with_label:
                self.seg_paths = tf.convert_to_tensor([json_data[k]['seg_path'] for k in json_data.keys()])
                key = list(json_data.keys())
        except:
            try: 
                json_data = img_folder_to_json(filename)
            except:
                json_data = img_list_to_json(filename)  

        key = list(json_data.keys())
        if with_item:
            self.items = tf.convert_to_tensor([json_data[k]['item'] for k in key])
        else:
            self.items = tf.convert_to_tensor(key)
        self.img_paths = tf.convert_to_tensor([json_data[k]['img_path'] for k in json_data.keys()])                

        # iteration 
        if no_obj_sampling_w is None:
            self.dataset = tf.data.Dataset.from_tensor_slices(np.arange(0, len(json_data)) )
        else:
            n_obj = np.where( np.array([len(json_data[k]['seg_path']) for k in json_data.keys()]) > 0, 1, 0).sum()
            n_nobj = len(json_data) - n_obj
            w_nobj  = n_obj * no_obj_sampling_w / n_nobj
            self.weight =[0] + [1 if json_data[k]['seg_path']!='' else w_nobj for k in json_data.keys() ]
            self.weight = np.cumsum(self.weight)       
            self.dataset = tf.data.Dataset.from_generator(self.generator, (tf.int64), (tf.TensorShape([])))
        
        self.dataset = self.dataset.map(lambda x: self.one_batch(x), 
                                        num_parallel_calls=num_threads).prefetch(1)
        if buffer_size <= 0:
            self.dataset = self.dataset.batch(batch_size)
        else:
            self.dataset = self.dataset.shuffle(buffer_size, reshuffle_each_iteration= True).batch(batch_size)
        self.iterator = self.dataset.make_initializable_iterator()
        self.next_op = self.iterator.get_next()
        self.max_iter = math.ceil(len(json_data)/batch_size)

    def initialize(self, sess):
        sess.run(self.iterator.initializer)
        
    def generator(self):
        return D.DataLoader.generator(self)
            
    def get_bbox(self, *args):
        return D.DataLoader.get_bbox(self, *args)

    def get_target(self, *args):
        return D.DataLoader.get_target(self, *args)

    def __next__(self, idx):
        return D.DataLoader.__next__(self, idx)

    def one_batch(self, idx):
        img = self.__next__(idx)
        item = self.items[idx]
        path = self.img_paths[idx]
        return img, item, path


class TrainDataLoader(DataLoader):
    def __init__(self, filename, img_size, n_sample, **kwargs):
        DataLoader.__init__(self,  filename, img_size, n_sample, **kwargs)

    def one_batch(self, idx):
        item = self.items[idx]

        pos_idx = tf.where(tf.equal(self.items, item))
        pos_idx1 = tf.where(tf.not_equal(pos_idx[:, 0], idx))[:, 0]
        pos_idx = tf.cond(tf.shape(pos_idx1)[0]>0, 
                  lambda: tf.gather(pos_idx, pos_idx1), 
                  lambda: pos_idx)
        pos_idx = tf.random_shuffle(pos_idx)[0:self.n_sample]
        pos_idx = tf.tile(pos_idx, [self.n_sample, 1])[0: self.n_sample]
        neg_idx = tf.random_shuffle(tf.where(tf.not_equal(self.items, item)))[0: self.n_sample]
        img_idx = tf.cast(tf.concat([pos_idx, neg_idx], 0)[:, 0], tf.int32)
        img_idx = tf.concat([tf.cast(idx, tf.int32)[None], img_idx], 0)

        imgs, instances, targets = [], [], []
        for i in range(self.n_sample*2 + 1):
            tf_img, tf_instance, tf_target = self.__next__(img_idx[i])
            imgs.append(tf_img[None])
            instances.append(tf_instance[None])
            targets.append(tf_target[None])
        imgs = tf.concat(imgs, 0)
        instances = tf.concat(instances, 0)
        targets = tf.concat(targets, 0)
        return imgs, instances, targets 