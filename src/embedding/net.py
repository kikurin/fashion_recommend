import tensorflow as tf
import tensorflow.contrib.slim as slim

from mrcnn.net import MaskRCNN

class Embedding(MaskRCNN):
    def __init__(self, **kwargs):
        MaskRCNN.__init__(self, **kwargs)
        self.emb =EmbeddingHead(self.n_feature, scope='embedding')
        
    def forward(self, x, is_training):
        prediction, pred_bbox, nms, nms_mask, nms_pred_mask_logit, nms_pred_mask, nms_pred_class, nms_pred_logit= MaskRCNN.forward(self, x, is_training)
        batch_size = tf.shape(prediction)[0]
        
        nms_emb = self.emb.forward(self.roi_feature_mask, self.roi_feature_class, is_training) #[B * n_nms, self.n_feature]
        nms_emb = tf.reshape(nms_emb, [batch_size, -1, self.n_feature]) #[B, n_nms, n_feature]
        return prediction, pred_bbox, nms, nms_mask, nms_pred_mask_logit, nms_pred_mask, nms_pred_class, nms_pred_logit, nms_emb

        
class EmbeddingHead():
    def __init__(self, n_feature= 256, scope='embedding', fix_feature = False):
        self.n_feature = n_feature
        self.scope = scope
        self.fix_feature = fix_feature

    def forward(self, roi_feature_mask, roi_feature_class = None, is_training = True):
        # roi_feature_mask, roi_feature_class: [?, pool_h, pool_w, C]
        with tf.variable_scope(self.scope):
            with slim.arg_scope([slim.conv2d], activation_fn=tf.nn.relu, 
                            normalizer_fn=tf.layers.batch_normalization,
                            normalizer_params={'training': is_training}):
                if self.fix_feature:
                    roi_feature_mask = tf.stop_gradient(roi_feature_mask) 
                emb_mask=slim.repeat(roi_feature_mask, 4, slim.conv2d, self.n_feature*4, [3, 3],  scope='conv_emb_mask')
                emb_mask = global_average_pool2d(emb_mask) #[-1, n_feature*4]
                if roi_feature_class is not None:
                    if self.fix_feature:
                        roi_feature_class = tf.stop_gradient(roi_feature_class)
                    emb_class =slim.repeat(roi_feature_class, 4, slim.conv2d, self.n_feature*4, [3, 3],  scope='conv_emb_class')
                    emb_class = global_average_pool2d(emb_class) #[-1, n_feature*4]
                    emb = tf.concat([emb_mask, emb_mask], 1)
                else:
                    emb = emb_mask
            emb = slim.fully_connected(emb, self.n_feature, scope='fc_emb', activation_fn = None) #[-1, n_feature]
        return emb
        
        
def global_average_pool2d(x):
    # x:[batch, H, W, C]
    return tf.reduce_mean(x, axis=[1, 2]) 
