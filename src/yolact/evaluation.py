import numpy as np

def evaluate_mAP(dats, n_class, iou_threshold=0.5):
    AP = []
    for c in range(0, n_class):
        TPs, TP_confs, trues = [], [], []
        for i in range(len(dats)):
            true_idx, pred_idx, TP_conf, ious_now = dats[i][c]
            TP = np.zeros(len(pred_idx))
            true_match = np.zeros(len(true_idx))
            if len(true_idx) > 0:
                for p in range(len(TP)):
                    ious = ious_now[p]
                    sorted_ixs =np.argsort(-np.array(ious))
                    for t in sorted_ixs:
                        if true_match[t]==1: # skip gt_box if it's already detected
                            continue 
                        if ious[t] < iou_threshold:
                            break               
                        true_match[t] = 1
                        TP[p] = 1       
                TPs = np.append(TPs, TP)
                TP_confs = np.append(TP_confs, TP_conf)
                trues = np.append(trues, true_match)        

        if len(trues)>0:   
            # recall-precision
            idx = np.argsort(-TP_confs)
            sort_TP = TPs[idx]
            precisions = np.cumsum(sort_TP)/(np.arange(len(TPs)) + 1)
            precisions = np.concatenate([[0], precisions, [0]])
            for i in range(len(precisions) - 2, -1, -1): # ensure precisions is decreasing
                precisions[i] = np.maximum(precisions[i], precisions[i + 1])
            recalls = np.cumsum(sort_TP) / len(trues)
            recalls = np.concatenate([[0], recalls, [1]])
    
            # when recall=r, adjust precision to max_precision for all recalls>=r 
            max_precision = []
            for r in np.unique(recalls):
                max_prec = np.max(precisions[ np.where(recalls>=r)])
                max_precision.append([r, max_prec])
    
            average_precision = 0 
            for i in range(1, len(max_precision)):
                average_precision += (max_precision[i][0] - max_precision[i-1][0]) * max_precision[i][1]
            AP.append(average_precision)
                
    mAP = np.mean(AP)
    return mAP

    
def arrange_single_mask_data(pred_mask, true_mask, pred_conf, n_class):
    '''
    Args:
        - true_mask: list, [t][H, W], with category values (0: background)
        - pred_mask: list, [p][H, W], with category values
        - pred_conf:  list, [p]
    Returns: list, [n_class][[true_idx, pred_idx, conf, ious]]
    '''
    dat = []
    pred_mask, true_mask, pred_conf = np.array(pred_mask), np.array(true_mask), np.array(pred_conf)
    ious_all =  iou_mask(np.where(pred_mask>0, 1, 0),  np.where(true_mask>0, 1, 0)) 
    for c in range(1, n_class+1):
        if len(true_mask)>0:
            true_idx = np.where(np.max(true_mask, -1).max(-1)==c)[0].tolist()
        else:
            true_idx = []
        if len(pred_mask)>0:
            pred_idx = np.where(np.max(pred_mask, -1).max(-1)==c)[0].tolist()
            conf = pred_conf[pred_idx].tolist()
        else:
            pred_idx = []
            conf = []
        try:
            ious_now = ious_all[pred_idx][:, true_idx]
        except:
            ious_now = []
        dat.append([true_idx, pred_idx, conf, ious_now])
    return dat


def iou_mask(mask1, mask2):
    '''
    Args:
        - mask1: np.array, [n1, H, W], binary mask
        - mask2: [n2, H, W], binary mask
    Returns:
        - iou_mask : [n1, n2]
    '''
    try:
        mask1 = mask1[:, None]
        mask2 = mask2[None]
        intersection = np.sum(mask1 +mask2==2, -1).sum(-1)
        union = np.sum(mask1 +mask2>=1, -1).sum(-1)
        iou_mask = intersection/(union + 1e-5)
    except:
        iou_mask = np.zeros([len(mask1), len(mask2)])
    return iou_mask



    
def arrange_single_bbox_data(pred_bbox, true_bbox, n_class):
    '''
        Args:
        - pred_box_list: [xmin, xmax, ymin, ymax, conf, class_p0, ...]
        - true_box_list: [xmin, xmax, ymin, ymax, class]
    '''
    dat = []
    true_bbox, pred_bbox = np.array(true_bbox), np.array(pred_bbox)
    ious_all = iou_bbox(pred_bbox[:, 0:4], true_bbox[:, 0:4])
    pred_conf = pred_bbox[:, 4]
    for c in range(n_class):
        if len(true_bbox)>0:
            true_idx = np.where(true_bbox[:, 4]==c)[0]
            pred_idx = np.where(np.argmax(pred_bbox[:, 5:])==c)[0]
            conf = pred_conf[pred_idx]
            try:
                ious_now = ious_all[pred_idx][:, true_idx].tolist()
            except:
                ious_now = []
            dat.append([true_idx.tolist(), pred_idx.tolist(), conf.tolist(), ious_now])
    return dat
        
    
def iou_bbox(bbox1, bbox2):
    '''
    Args: 
        - bbox1: [b1, 4]
        - bbox2: [b2, 4]
    Returns:
        - iou: [b1, b2]
    '''
    bbox1 = bbox1[:, None]
    bbox2= bbox2[None]

    mins1, maxs1 = bbox1[..., 0:2], bbox1[..., 2:4]
    mins2, maxs2 = bbox2[..., 0:2], bbox2[..., 2:4]
    overlap_mins = np.maximum(mins1, mins2)
    overlap_maxs = np.minimum(maxs1, maxs2)
    overlap_area = np.prod(overlap_maxs - overlap_mins, -1)
    area1 = np.prod(maxs1 - mins1, -1)
    area2 = np.prod(maxs2 - mins2, -1)
    iou = overlap_area/(area1+area2 -overlap_area +1e-5)
    return iou
