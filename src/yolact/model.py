import os
import tensorflow as tf
import numpy as np
from tqdm import trange
from IPython import display
from tensorflow.python import pywrap_tensorflow
import re

from .net import Yolact
from .dataloader import DataLoader
from .loss import Loss, get_bbox_mask
from .net import get_pred_mask
from fovea.tf_utils import get_batch_nms
from utils import log as L
from .evaluation import evaluate_mAP, arrange_single_mask_data
from fovea.model import poly_decay, flat


class ModelBase():
    def __init__(self, config, img_size, n_class, reset_graph= True, **net_args):    
        self.config = config
        self.img_size = H, W = img_size
        self.n_class = n_class    
        if reset_graph:
            tf.reset_default_graph()
        self.graph = tf.Graph()    
        with self.graph.as_default():
            with tf.name_scope('network'):
                self.img = tf.placeholder(tf.float32, [None, H, W, 3])
                self.is_training = tf.placeholder(tf.bool, [])
                self.define_network(**net_args)
            self.saver = tf.train.Saver()              
            
    def define_network(self, **net_args):
        self.net = Yolact(self.n_class, **net_args)
        self.pred, self.pred_bbox, self.nms, self.nms_mask, self.pred_M_logit, self.pred_M, self.nms_pred_mask, self.nms_pred_class = self.net.forward(self.img, self.is_training)
        self.coder = self.net.coder      


    def define_target(self):
        with self.graph.as_default():
            with tf.name_scope('target'):
                self.gt_box = tf.placeholder(tf.float32, [None, None, 6])
                self.instance = tf.placeholder(tf.float32, [None] + self.img_size)
                self.target = self.coder.encode(self.gt_box)
                self.true_bbox = self.coder.decode(self.target)
                self.target, self.true_bbox = flat(self.target), flat(self.true_bbox)

    def save_model(self, sess, save_path):
        self.saver.save(sess, save_path + '/model.ckpt') 
    
    def restore_model(self, sess, restore_path, pretrained=False):
        if pretrained: 
            reader = pywrap_tensorflow.NewCheckpointReader(restore_path +'/model.ckpt')
            reader_var = list(reader.get_variable_to_shape_map().keys())
            load_var = self.saver._var_list
            assign_op=[]
            not_loading = []
            for r in reader_var:
                l = [l for l in load_var if re.findall(r, l.name)!=[]]
                value = reader.get_tensor(r)
                l  = [ll for ll in l if ll.shape.as_list()  == list(value.shape)]
                if len(l)==1:
                    assign_op += [tf.assign(l[0], reader.get_tensor(r))]
                else:
                    not_loading.append(r)
            _  = sess.run(assign_op)
            print('variable not been loaded: ', not_loading)          
        else:
            self.saver.restore(sess, restore_path + "/model.ckpt")
            
    def define_dataset(self, filename, **kwargs):
        with self.graph.as_default():
            self.dat = DataLoader(filename, self.img_size, **kwargs)     

                
    def get_pred_mask_value(self, pred_mask, pred_class):
        pred_mask = [[(m>0.5) *(np.argmax(p)+1) for m, p in zip(mask, p_class)  if np.max(p)>0 
                    ]for mask, p_class in zip(pred_mask, pred_class)]
        return pred_mask                 




class ModelTrain(ModelBase):
    def __init__(self, base_args, data_args, loss_args, opt_args):
        ModelBase.__init__(self, **base_args)
        self.define_dataset(**data_args)
        self.define_loss(**loss_args)
        self.define_optimizer(**opt_args)
        
        
    def define_dataset(self, train_filename, valid_filename, **kwargs):
        with self.graph.as_default():
            self.train_dat = DataLoader(train_filename, self.img_size, **kwargs)
            self.valid_dat = DataLoader(valid_filename, self.img_size, **kwargs)
            
    def define_loss(self, focal_alpha = 0.25, focal_gamma=2, smooth_l1_beta=0.11, iou_threshold=0.5):
        with self.graph.as_default():
            self.define_target()
            loss_fn = Loss(focal_alpha, focal_gamma, smooth_l1_beta, iou_threshold,
                            self.net.obj_thresh, self.net.nms_thresh, self.net.nms_max_n)
            with tf.name_scope('loss'):
                self.loss_dict = loss_fn(self.target, self.pred, self.instance, self.pred_M, self.pred_M_logit, 
                                         self.true_bbox, self.pred_bbox, self.nms, self.nms_mask)        
        
    def define_optimizer(self, init_lr, lr_decay, alpha_l2, alpha_bbox, alpha_class, alpha_mask, 
                    alpha_nms_class = None, momentum=0.9):
        with self.graph.as_default():    
            with tf.name_scope('loss'):
                loss = alpha_l2 * tf.reduce_mean([tf.nn.l2_loss(v) for v in tf.trainable_variables()])+ \
                        alpha_class * self.loss_dict['loss_class'] +\
                        alpha_bbox * self.loss_dict['loss_bbox'] +\
                        alpha_mask * self.loss_dict['loss_mask']
                if alpha_nms_class is not None:
                    loss += alpha_nms_class * self.loss_dict['loss_nms_class']     
                self.loss_dict['loss'] = loss       
            with tf.name_scope('optimizer'):
                self.init_lr = init_lr
                self.lr_decay = lr_decay
                self.lr = tf.placeholder(tf.float32, [])
                opt = tf.train.MomentumOptimizer(self.lr, momentum)  
                v = tf.trainable_variables()
                g = tf.gradients(loss, v)
                #g = tf.clip_by_global_norm(g, 5)[0]
                opt = opt.apply_gradients(list(zip(g,v)))
                self.opt = tf.group([opt, tf.get_collection(tf.GraphKeys.UPDATE_OPS)])   
                
        
                
    def one_epoch(self, sess, dat, epoch_size, lr, train = False):
        summary = np.zeros(len(self.loss_dict.keys()))
        dats = []  
        sess_op = [self.loss_dict, self.nms_pred_mask, self.nms_pred_class]
        if train:
            sess_op += [self.opt]
        
        for _ in trange(epoch_size):
            try:
                img, instance, gt_box = sess.run(dat.next_op)
            except:
                dat.initialize(sess) 
                img, instance, gt_box = sess.run(dat.next_op)    
            feed_dict = {self.img: img, self.instance: instance, self.gt_box: gt_box, self.is_training: True,
                         self.lr: lr}
            sess_value= sess.run(sess_op, feed_dict)
            loss_dict = sess_value[0]
            summary += list(loss_dict.values()) 
            # mAP data
            pred_mask, pred_class = sess_value[1:3]            
            true_mask = [[(ins==b[5]) * (b[4]+1) for b in box if b[-1]>0] for box, ins in zip(gt_box, instance)]
            pred_mask = self.get_pred_mask_value(pred_mask, pred_class)            
            pred_obj = [[np.max(p) for p in p_class if p.sum()>0] for p_class in pred_class]
            dats += [arrange_single_mask_data(p, t, conf, self.n_class) for p, t, conf in zip(pred_mask, true_mask, pred_obj)]   

        summary /= epoch_size
        mAP= evaluate_mAP(dats, self.n_class, 0.5) 
        return summary.tolist() +[mAP]

    def train(self, epoch_n, save_epoch_n, train_epoch_size, valid_epoch_size, save_path, 
              restore_path = None, restore_pkl = None, show_fig_start = 0, 
              valid_ave_n = 5, pretrained=False):
        os.makedirs(save_path, exist_ok = True)
        log = L.Log(list(self.loss_dict.keys()) +['mAP'] , save_path, valid_ave_n)
        best_loss = 0
        
        with tf.Session(graph = self.graph, config =self.config) as sess:
            init = [tf.global_variables_initializer(), tf.local_variables_initializer()]
            sess.run(init)
            self.train_dat.initialize(sess)
            self.valid_dat.initialize(sess)
            
            if restore_pkl is not None:
                self.net.backbone.load_pkl(sess, restore_pkl)
            if restore_path is not None: 
                self.restore_model(sess, restore_path, pretrained)
            self.graph.finalize()
            
            for epoch in trange(epoch_n):
                lr_now = poly_decay(self.init_lr, self.lr_decay, epoch, epoch_n)
                train_summary = self.one_epoch(sess, self.train_dat, train_epoch_size, lr_now , True)
                valid_summary = self.one_epoch(sess, self.valid_dat, valid_epoch_size, lr_now , False)
                
                for i, j in enumerate(log.var_names):
                    log.update_value(epoch, j, train_summary[i], valid_summary[i])
                log.update_log(epoch)
                display.clear_output(wait=True)
                log.update_plot(show_fig_start)
            
                if epoch % save_epoch_n==0:
                    self.save_model(sess, save_path)
                
                loss_now = -log.evaluation['ave_valid_mAP'][epoch]
                if loss_now< best_loss:
                    best_loss = loss_now
                    self.save_model(sess, save_path + '/best')               
            
            
                        