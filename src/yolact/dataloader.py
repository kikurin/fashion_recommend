import os
import tensorflow as tf
import numpy as np
import math
import itertools
import json

from utils.img_augmentation import random_pad, random_crop, flip_left_right 
from deeprank.dataloader import img_folder_to_json, img_list_to_json

def instance2label(instance):
    return tf.clip_by_value(instance//100-1, 0, 100)
    

class DataLoader():
    def __init__(self, filename, img_size, batch_size, buffer_size, num_threads, n_max_box, data_aug, with_label, no_obj_sampling_w=None):
        # if no_obj_sampling_w=1, same weight as obj     
        self.img_size = img_size
        self.data_aug = data_aug
        self.with_label = with_label
        self.n_max_box = n_max_box       
        try:
            json_data = json.load(open(filename)) 
            if with_label:
                self.seg_paths = tf.convert_to_tensor([json_data[k]['seg_path'] for k in json_data.keys()])
        except:
            try: 
                json_data = img_folder_to_json(filename)
            except:
                json_data = img_list_to_json(filename)  
        self.img_paths = tf.convert_to_tensor([json_data[k]['img_path'] for k in json_data.keys()])                

        # iteration 
        if no_obj_sampling_w is None:
            self.dataset = tf.data.Dataset.from_tensor_slices(np.arange(0, len(json_data)) )
        else:
            n_obj = np.where( np.array([len(json_data[k]['seg_path']) for k in json_data.keys()]) > 0, 1, 0).sum()
            n_nobj = len(json_data) - n_obj
            w_nobj  = n_obj * no_obj_sampling_w / n_nobj
            self.weight =[0] + [1 if json_data[k]['seg_path']!='' else w_nobj for k in json_data.keys() ]
            self.weight = np.cumsum(self.weight)       
            self.dataset = tf.data.Dataset.from_generator(self.generator, (tf.int64), (tf.TensorShape([])))
        
        self.dataset = self.dataset.map(lambda x: self.__next__(x), 
                                        num_parallel_calls=num_threads).prefetch(1)
        if buffer_size <= 0:
            self.dataset = self.dataset.batch(batch_size)
        else:
            self.dataset = self.dataset.shuffle(buffer_size, reshuffle_each_iteration= True).batch(batch_size)
        self.iterator = self.dataset.make_initializable_iterator()
        self.next_op = self.iterator.get_next()
        self.max_iter = math.ceil(len(json_data)/batch_size)

    def initialize(self, sess):
        sess.run(self.iterator.initializer)
        
    def generator(self):
        for i in itertools.count(1):
            random_p = np.random.uniform(0, self.weight[-1])
            idx = np.where(self.weight<=random_p)[0][-1]
            yield idx 
            
    def get_bbox(self, seg, l):
        '''
            Args: 
                - seg: segmentation mask, [H, W]
                - l: label, []
            Returns:
                - target: [?, 6(xmin, ymin, xmax, ymax, class_label, instance_label)]
        '''
        maxs = tf.reduce_max(tf.where(tf.equal(seg, l)), 0)[::-1] #yx --> xy
        mins = tf.reduce_min(tf.where(tf.equal(seg, l)), 0)[::-1]
        c = tf.cast(tf.expand_dims(instance2label(l), 0), tf.int32)
        target = []
        for v in (mins, maxs, c, tf.expand_dims(l, 0)):
            target.append(tf.cast(v, tf.float32))
        return tf.concat(target, 0) 

    def get_target(self, seg):
        label, _= tf.unique(tf.reshape(seg, [-1]))
        label = tf.gather(label, tf.where(label>0))[0:self.n_max_box, 0] #exclude label = 0
        tf_target = tf.cond(tf.shape(label)[0]>0, 
                    lambda: tf.map_fn(lambda l:  tf.cast(self.get_bbox(seg, l), tf.float32), tf.cast(label, tf.float32)), 
                    lambda: tf.zeros([self.n_max_box, 6], tf.float32)-1)
        n_gt = tf.shape(tf_target)[0]
        tf_target = tf.concat([tf_target, -1 + tf.zeros(shape=[self.n_max_box - n_gt, 6])], 0) #[self.n_max_box, 6]
        return tf_target

    def __next__(self, idx):
        img_name = self.img_paths[idx]
        tf_img =  tf.image.decode_png(tf.read_file(img_name), channels=3)
        if self.with_label:
            seg_name = self.seg_paths[idx]
            tf_seg = tf.cond(tf.equal(seg_name, ''), 
                         lambda: tf.zeros_like(tf_img, dtype=tf.uint16), 
                         lambda: tf.image.decode_png(tf.read_file(seg_name), channels=1, dtype=tf.uint16)        
                    )
            tf_img =tf.concat([tf.cast(tf_img, tf.float32), tf.cast(tf_seg, tf.float32)], -1)

        origin_size = tf.cast(tf.shape(tf_img), dtype=tf.float32)[0:2]
        resize_ratio =  tf.random_uniform([], 0.5, 1.5) if self.data_aug else 1
        resize_factor = tf.reduce_min(self.img_size /origin_size, 0) * resize_ratio
        resize_shape = tf.cast(resize_factor * origin_size, tf.int32)
        aug_img =  tf.image.resize_images(tf_img, resize_shape,
                                        method=tf.image.ResizeMethod.NEAREST_NEIGHBOR, 
                                        align_corners= True)
        aug_size = tf.cast(tf.shape(aug_img), dtype=tf.float32)[0:2]
        if self.data_aug:
            # random_pad
            aug_img, _, _ = tf.cond(
                tf.reduce_min(aug_size/self.img_size)<1, 
                lambda: random_pad(aug_img, self.img_size, aug_size), 
                lambda: (aug_img, 0, 0)
            )   
            # random_crop
            aug_size = tf.cast(tf.shape(aug_img), dtype=tf.float32)[0:2]
            aug_img, _, _ = random_crop(aug_img, self.img_size, aug_size)
            aug_img = tf.cond(tf.random_uniform([], 0, 1) > 0.5,
                lambda: flip_left_right(aug_img, None),
                lambda: aug_img)
        else:
            aug_img, _, _ = random_pad(aug_img, self.img_size, aug_size, random = False) 

        tf_img = aug_img[..., 0:3]
        mean = tf.constant([0.485, 0.456, 0.406], dtype = tf.float32)
        std = tf.constant([0.229, 0.224, 0.225], dtype=tf.float32)
        tf_img = (tf_img/255 - mean)/std
        if self.with_label:
            tf_instance = aug_img[..., 3]
            tf_target =  self.get_target(tf_instance)
            return tf_img, tf_instance, tf_target #[H, W, 3], [H, W], [n_max_box, 6(x1, y1, x2, y2, label, instance_id)]
        else:
            return tf_img