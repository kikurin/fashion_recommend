import tensorflow as tf
from fovea.tf_utils import get_batch_nms, get_meshgrid
from fovea.loss import ious_over_matrix, Loss as FoveaLoss


class Loss(FoveaLoss):
    def __init__(self, *kwargs):
        FoveaLoss.__init__(self, *kwargs)
    
    def __call__(self, target, pred, instance, pred_M, pred_M_logit, true_bbox, pred_bbox, nms, nms_mask):
        '''
        Args:
            - target: [batch_size, ?, 4+n_class], encoded values
            - pred: [batch_size, ?, 4+n_class+n_mask], output from network
            - instance: [batch_size, H, W]
            - pred_M, pred_M_logit:  [batch_size, n_nms, mask_h, mask_w]
            - true_bbox, pred_bbox: [batch_size, ?, 4], original(decoded) bbox's coordinates
            - nms:  #[batch_size, nms_max_n, 2(batch_size, box_id)]
            - nms_mask: [batch_size, n_nms, 1]
        '''
        batch_size = tf.shape(target)[0]
        h, w = instance.shape.as_list()[1:3]
        mask_h, mask_w = pred_M.shape.as_list()[2:]
        n_class = target.shape.as_list()[-1]-4
        
        t_offset, labels = target[..., 0:4], target[..., 4:4+n_class]
        p_offset, logits = pred[..., 0:4], pred[..., 4:4+n_class]
        pred_class = tf.sigmoid(logits)
        pred_obj = tf.reduce_max(pred_class, -1)
        
        # loss_class
        class_mask = tf.cast(labels>=0, tf.float32)
        loss_class = self.focal_loss(logits, labels)
        loss_class = tf.reduce_sum(loss_class * class_mask)/tf.reduce_sum(class_mask)
        
        # loss_offset 
        bbox_mask = tf.reduce_max(tf.cast(tf.abs(labels)>0, tf.float32), -1) #[batch_size, ?]
        n_bbox = tf.reduce_sum(bbox_mask) + 1e-5
        loss_bbox = self.smooth_loss(t_offset, p_offset)
        loss_bbox = tf.reduce_sum(loss_bbox * bbox_mask[..., None])/n_bbox    
    
        # nms
        float_nms_mask = tf.cast(nms_mask, tf.float32)
        true_nms_bbox = tf.gather_nd(true_bbox, nms) * float_nms_mask  

        # loss_mask
        #pred_M_logit, pred_M = get_pred_mask(pred_bbox, p_mask_coef, proto_mask, nms)
        true_M, true_bbox_M = get_true_mask(true_nms_bbox, instance, mask_h, mask_w)
        loss_mask = calculate_loss_mask(true_M, pred_M_logit, nms_mask, true_bbox_M)   
        
        # recall
        ious = ious_over_matrix(true_bbox, pred_bbox) *bbox_mask
        obj_mask = tf.cast(pred_obj *bbox_mask > self.obj_thresh, tf.float32)
        hit_bbox_mask = tf.cast(ious > self.iou_thresh, tf.float32) 
        recall_bbox = tf.reduce_sum(hit_bbox_mask)/n_bbox
        recall = tf.reduce_sum(hit_bbox_mask * obj_mask)/n_bbox                
        recall_mask = tf.cast(tf.equal(true_M, tf.cast(pred_M>0.5, tf.float32)), tf.float32) * true_bbox_M
        recall_mask = tf.reduce_sum(recall_mask)/(tf.reduce_sum(true_bbox_M) + 1e-5)
        
        loss_dict =  {'loss_bbox': loss_bbox, 'loss_class': loss_class, 'loss_mask': loss_mask, 
                      'recall': recall, 'recall_bbox': recall_bbox, 'recall_mask': recall_mask}
        
        return loss_dict


def calculate_loss_mask(true_mask, pred_logit, nms_mask, true_bbox_mask=None):
    # true_mask, pred_logit, true_bbox_mask: tf.float32, [batch_size, n_nms, mask_h, mask_w]
    # nms_mask: tf.int32, [batch_size, n_nms, 1]
    nms_mask = tf.cast(nms_mask[..., None], tf.float32)
    mask_h, mask_w = true_mask.shape.as_list()[2:]
    loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=true_mask, logits = pred_logit)
    loss *= nms_mask    
    if true_bbox_mask is not None:
        loss *= true_bbox_mask
        loss = tf.reduce_sum(loss, axis=[2, 3])/tf.reduce_sum(true_bbox_mask, axis=[2, 3])

    loss = tf.reduce_sum(loss)/(tf.reduce_sum(nms_mask)+1e-5)
    return loss



def get_bbox_mask(bbox, mask_h, mask_w):
    '''
    Args: 
        -bbox: tf.float32, [..., 4(xmin, ymin, xmax, ymax)]
        - mask_h, mask_w: int or tf.int32
    Returns:
        - bbox_mask: binary mask indicating bounding box area
    '''
    
    
    mins, maxs = bbox[..., 0:2], bbox[..., 2:4] #[...,  2]
    grid_xy = get_meshgrid(mask_h, mask_w, tf.float32) #[mask_h, mask_w, 2]
    for _ in range(len(mins.shape.as_list()) - 1):
        grid_xy = tf.expand_dims(grid_xy, 0) #[..., mask_h, mask_w, 2]
        mins = tf.expand_dims(mins, -2) #[..., 1, 1, 2]
        maxs = tf.expand_dims(maxs, -2)   
    bbox_mask = tf.cast(grid_xy >=mins, tf.float32) * tf.cast(grid_xy <=maxs, tf.float32)
    bbox_mask = tf.reduce_prod(bbox_mask, -1) #[..., mask_h, mask_w]   
    return bbox_mask


def get_true_mask(bbox, instance, mask_h, mask_w):
    '''
    Args:
        - bbox: decoded bbox coordinates, tf.float32, [batch_size, ?, 4]
        - instance: instanceid, tf.float32, [batch_size, H, W]  
    Returns:
        - true_mask, bbox_mask: [batch_size, ?, mask_h, mask_w]
    '''  
    h, w = instance.shape.as_list()[1:3]
    batch_size, n_box = tf.shape(bbox)[0], tf.shape(bbox)[1]

    ratio_h, ratio_w = mask_h/h, mask_w/w
    bbox *= [ratio_w, ratio_h, ratio_w, ratio_h]
    bbox_mask = get_bbox_mask(bbox, mask_h, mask_w)# #[batch_size, n_nms, mask_h, mask_w] 
    
    resize_instance = tf.image.resize_bilinear(instance[..., None], [mask_h, mask_w])[..., 0] #[batch_size, mask_h, mask_w]
    mask = resize_instance[:, None] * tf.cast(bbox_mask>0, tf.float32) #[batch_size, ?, mask_h, mask_w]
    
    def get_single_mask(true_mask):
        # true_mask: [H, W]
        idx, _, counts = tf.unique_with_counts(tf.reshape(true_mask, [-1]))
        instance_id = idx[tf.nn.top_k(counts * tf.cast(idx>0, tf.int32), 1).indices[0]]
        mask = tf.cast(tf.equal(true_mask, instance_id), tf.int32)
        return mask
    
    true_mask = tf.map_fn(lambda b: 
                tf.map_fn(lambda i: get_single_mask(mask[b, i]), tf.range(n_box)), 
                tf.range(batch_size)) #[batch_size, n_box]

    return tf.cast(true_mask, tf.float32), bbox_mask