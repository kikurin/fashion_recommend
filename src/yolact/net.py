import tensorflow as tf
import tensorflow.contrib.slim as slim
from fovea.backbone_net import YoloV3, Resnet
from fovea.net import FPN
from fovea.tf_utils import FoveaCoder, get_batch_nms
from fovea.model import flat
from .loss import get_bbox_mask

class Yolact():
    def __init__(self, n_class, n_feature, n_mask, strides, normalize_factors,  scale_ranges,  
                 pos_shrink = 0.3, neg_shrink=0.4, 
                 nms_max_n=10, nms_threshold=0.5, obj_threshold=0.5, network = 'Resnet', **backbone_args):
        if network in ('Resnet', 'resnet'):
            self.backbone = Resnet(scope='backbone', **backbone_args)
        else:
            self.backbone = YoloV3(scope='backbone', **backbone_args)
        self.n_class = n_class
        self.fpn = FPN(n_feature, n_downsample_layer = 2, scope='FPN')
        self.predhead = PredictHead(n_class, n_feature, n_mask)
        self.proto_net = ProtoNet(n_feature, n_mask)
        self.coder_args = dict(strides = strides , normalize_factors = normalize_factors,  
                                scale_ranges = scale_ranges, n_class = n_class, 
                                pos_shrink  = pos_shrink, neg_shrink = neg_shrink)
        self.nms_max_n = nms_max_n
        self.nms_thresh= nms_threshold
        self.obj_thresh = obj_threshold

    def forward(self, x, is_training):
        '''
        Args:
            - x: tf.float32, [batch_size, H, W, C]
            - is_training: tf.bool or bool
        Returns:
            - prediction: [batch_size, n_box, 4+n_class+n_mask_coef]
            - pred_bbox: decoded box's coordinates, [batch_size, n_box, 4(xmin, ymin, xmax, ymax)]
            - nms: [batch_size, n_nms, 2(batch_idx, box_idx)]
            - nms_mask: [batch_size, n_nms, 1]
            - pred_M_logit, pred_M: [batch_size, n_nms, mask_h, mask_w]
            - pred_nms_mask: [batch_size, n_nms, H, W]
            - nms_pred_class: probability, [batch_size, n_nms, n_class]
        '''        
        H, W = x.shape.as_list()[1:3]
        out = self.backbone.forward(x, is_training)
        fpn_out = self.fpn.forward(out[-3:])
        self.conv_dim = [c.shape.as_list()[1:3] for c in fpn_out]
        self.coder = FoveaCoder(self.conv_dim, **self.coder_args)
        prediction = self.predhead.forward(fpn_out) #[?, conv_h, conv_w, 4 + n_class +n_mask_coef]
        proto_mask = self.proto_net.forward(fpn_out[0]) #[?, conv_h, conv_w, n_mask]]

        pred_bbox = self.coder.decode(prediction)
        pred_bbox = flat(pred_bbox)
        prediction = flat(prediction)
        pred_class = tf.sigmoid(prediction[..., 4:4+self.n_class])
        pred_obj = tf.reduce_max(pred_class, -1)
        
        # nms
        nms = get_batch_nms(pred_bbox, pred_obj, self.nms_max_n, self.nms_thresh, self.obj_thresh) #[batch_size, nms_max_n, 2(batch_id, box_idx)]
        nms_mask =tf.where(nms[..., 1] >0, tf.ones_like(nms[..., 1]), tf.zeros_like(nms[..., 1]))[..., None] #[batch_size, nms_max_n, 1]
        nms *= nms_mask 
        
        # mask, class     
        pred_M_logit, pred_M = get_pred_mask(pred_bbox, prediction[..., 4+self.n_class:], proto_mask, nms)
        pred_nms_bbox = tf.gather_nd(pred_bbox, nms) * tf.cast(nms_mask, tf.float32)
        pred_bbox_mask = get_bbox_mask(pred_nms_bbox[..., 0:4], H, W)        
        nms_pred_mask = tf.transpose(
                        tf.image.resize_bilinear(tf.transpose(pred_M, (0, 2, 3, 1)), [H, W]), 
                    (0, 3, 1, 2)) * pred_bbox_mask #[batch_size, n_nms, H, W]
        nms_pred_class = tf.gather_nd(pred_class, nms) * tf.cast(nms_mask, tf.float32) #[batch_size, n_nms, n_class]   
        return prediction, pred_bbox, nms, nms_mask, pred_M_logit, pred_M, nms_pred_mask, nms_pred_class


    
class ProtoNet():
    def __init__(self, n_feature = 256, n_mask = 32, scope='ProtoNet'):
        self.scope = scope
        self.n_feature = n_feature
        self.n_mask = n_mask

    def forward(self, x):
        # x: tensor of [B, H, W, C]
        with tf.variable_scope(self.scope):
            for i in range(3):
                x = slim.conv2d(x, self.n_feature, [3, 3], scope='conv_%d'%i, activation_fn = tf.nn.relu)
            size = [s*2 for s in x.shape.as_list()[1:3]]
            x =  tf.nn.relu(tf.image.resize_bilinear(x, size))
            x = slim.conv2d(x, self.n_feature, [3, 3], scope='conv_%d'%(i+1), activation_fn = tf.nn.relu)
            x = slim.conv2d(x, self.n_mask, [3, 3], scope='conv_%d'%(i+2), activation_fn = tf.nn.relu)
        return x

class PredictHead():
    def __init__(self, n_class, n_feature = 256, n_mask_coef = 32, scope='Prediction'):
        self.n_class = n_class
        self.n_feature = n_feature
        self.scope =scope
        self.n_mask_coef = n_mask_coef

    def forward(self, feature_list):
        # feature_list: lists of m features map([B, H, W, C]) from higher to lower resolution (output from FPN) 
        c = self.n_class
        k = self.n_mask_coef
        pred = []
        for i, x in enumerate(feature_list):
            with tf.variable_scope(self.scope, reuse=tf.AUTO_REUSE):
                x_bbox  = slim.repeat(x, 4, slim.conv2d, self.n_feature, [3, 3],  scope='conv_feature_bbox', activation_fn=tf.nn.relu)    
                x_logit= slim.repeat(x, 4, slim.conv2d, self.n_feature, [3, 3],  scope='conv_feature_logit', activation_fn=tf.nn.relu)    
                x_bbox = slim.conv2d(x_bbox, 4, [3, 3], scope='conv_bbox', activation_fn = None) 
                x_logit = slim.conv2d(x_logit, c, [3, 3], scope='conv_logit', activation_fn = None)
                if self.n_mask_coef is not None:
                    x_mask= slim.repeat(x, 4, slim.conv2d, self.n_feature, [3, 3],  scope='conv_feature_mask', activation_fn=tf.nn.relu)    
                    x_mask =  slim.conv2d(x_mask, k, [3, 3], scope='conv_mask', activation_fn = tf.tanh)
                    pred.append(tf.concat([x_bbox, x_logit, x_mask], -1))
                else:
                    pred.append(tf.concat([x_bbox, x_logit], -1))
                
        return  pred #[scale][b h, w, 4+c+k]

    
    
def get_pred_mask(bbox, p_mask_coef, proto_mask, nms):
    '''
    Args:
        - bbox: decoded bbox coordinates, tf.float32, [batch_size, n_box, 4]
        - p_mask_coef: tf.float32, [batch_size, n_box, n_mask]
        - proto_mask: tf.float32, [batch_size, mask_h, mask_w, n_mask]
        - nms: tf.int32, [batch_size, n_nms, 2(batch_idx, box_idx)]
    Returns:
        - pred_M_logit, pred_M: [batch_size, n_nms, mask_h, mask_w]
    '''
    _, mask_h, mask_w, n_mask = proto_mask.shape.as_list()
    n_nms = tf.shape(nms)[1]
    
    C = tf.gather_nd(p_mask_coef, nms) #[batch_size, n_nms, n_mask]
    P = tf.reshape(proto_mask, [-1, mask_h * mask_w,  n_mask]) #[batch_size, mask_h * mask_w, n_mask]
    M = tf.matmul(P, C, transpose_b = True) #[batch_size, mask_h*mask_w, n_nms]
    pred_M_logit = tf.transpose(tf.reshape(M, [-1, mask_h, mask_w, n_nms]), [0, 3, 1, 2]) 
    pred_M = tf.sigmoid(pred_M_logit)
    return pred_M_logit, pred_M

