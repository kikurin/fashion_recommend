import tensorflow as tf

from fovea.loss import ious_over_matrix, Loss as FoveaLoss
from yolact.dataloader import instance2label
from yolact.loss import get_bbox_mask

class Loss(FoveaLoss):
    def __init__(self, *kwargs):
        FoveaLoss.__init__(self, *kwargs)
    
    def __call__(self, target, pred, instance, nms_pred_mask_logit, true_bbox, pred_bbox, nms, nms_mask, nms_pred_logit=None):
        epsilon = 1e-5
        '''
        Args:
            - target: [batch_size, ?, 4+n_class], encoded values
            - pred: [batch_size, ?, 4+n_class], output from network
            - instance: [batch_size, H, W]
            - nms_pred_mask_logit: [batch_size, n_max_nms, h, w, n_class]
            - true_bbox, pred_bbox: [batch_size, ?, 4], original(decoded) bbox's coordinates
            - nms:  #[batch_size, nms_max_n, 2(batch_size, box_id)]
            - nms_mask: [batch_size, n_nms, 1]
            - nms_pred_logit: [batch_size, n_nms, n_class+1]
        '''
        batch_size = tf.shape(target)[0]
        n_nms, h, w= nms_pred_mask_logit.shape.as_list()[1:4] 
        n_class = pred.shape.as_list()[2] -4
        float_nms_mask = tf.cast(nms_mask, tf.float32)
        
        t_offset, labels = target[..., 0:4], target[..., 4:4+n_class]
        p_offset, logits = pred[..., 0:4], pred[..., 4:4+n_class]
        pred_class = tf.sigmoid(logits)
        pred_obj = tf.reduce_max(pred_class, -1)
            
        # loss_class
        class_mask = tf.cast(labels>=0, tf.float32)
        loss_class = self.focal_loss(logits, labels)
        loss_class = tf.reduce_sum(loss_class * class_mask)/tf.reduce_sum(class_mask)
        
        if nms_pred_logit is None:
            loss_nms_class = tf.zeros([])
        else:
            # -1: neutral, 1: positive
            nms_labels = tf.gather_nd( tf.cast(tf.not_equal(labels, 0), tf.float32), nms)
            background =  tf.cast(tf.equal(tf.reduce_max(nms_labels, -1, keepdims = True), 0), tf.float32)
            nms_labels = tf.concat((nms_labels, background), -1)
            loss_nms_class = tf.losses.softmax_cross_entropy(nms_labels, nms_pred_logit, reduction = 'none')  #[batch_size, n_nms, n_class]
            loss_nms_class = tf.reduce_sum(loss_nms_class * float_nms_mask[..., 0])/tf.reduce_sum(float_nms_mask + epsilon)
                    
        # loss_offset 
        bbox_mask = tf.reduce_max(tf.cast(tf.abs(labels)>0, tf.float32), -1) #[batch_size, ?]
        n_bbox = tf.reduce_sum(bbox_mask) + epsilon
        loss_bbox = self.smooth_loss(t_offset, p_offset)
        loss_bbox = tf.reduce_sum(loss_bbox * bbox_mask[..., None])/n_bbox    

        # nms
        pred_nms_bbox = tf.gather_nd(pred_bbox, nms) * float_nms_mask
        true_nms_bbox = tf.gather_nd(true_bbox, nms) * float_nms_mask  

        # loss_mask
        true_mask = instance2label(instance + 100) #0: background      
        true_mask = tf.one_hot(tf.cast(true_mask, tf.int32), n_class+1)[..., 1:] #[batch_size, h, w, n_class]
        true_mask = tf.tile(true_mask[:, None], [1, n_nms, 1, 1, 1]) * get_bbox_mask(pred_nms_bbox, h, w)[..., None]
        mask_count = tf.reduce_sum(true_mask, [2, 3])
        max_mask_count = tf.equal(mask_count,  tf.reduce_max(mask_count, -1, keepdims=True)) #[batch_size, n_nms, n_class]
        loss_mask = tf.nn.sigmoid_cross_entropy_with_logits(labels=true_mask, logits = nms_pred_mask_logit)
        loss_mask *= tf.cast(max_mask_count[:, :, None, None, :], tf.float32)
        loss_mask = tf.reduce_sum(tf.reduce_mean(loss_mask, [2, 3]) *float_nms_mask 
                                 )/(tf.reduce_sum(float_nms_mask)+epsilon)
        
        # recall
        ious = ious_over_matrix(true_bbox, pred_bbox) *bbox_mask
        obj_mask = tf.cast(pred_obj *bbox_mask > self.obj_thresh, tf.float32)
        hit_bbox_mask = tf.cast(ious > self.iou_thresh, tf.float32) 
        recall_bbox = tf.reduce_sum(hit_bbox_mask)/n_bbox
        recall = tf.reduce_sum(hit_bbox_mask * obj_mask)/n_bbox                
        recall_mask = tf.cast(tf.equal(true_mask, tf.cast(nms_pred_mask_logit>0.5, tf.float32)), tf.float32)
        recall_mask = tf.reduce_sum(recall_mask*true_mask)/(tf.reduce_sum(true_mask)+epsilon)

        loss_dict =  {'loss_bbox': loss_bbox, 'loss_class': loss_class, 'loss_mask': loss_mask, 
                        'recall': recall, 'recall_bbox': recall_bbox, 'recall_mask': recall_mask, 
                        'loss_nms_class': loss_nms_class}
        return loss_dict       
                        
        


