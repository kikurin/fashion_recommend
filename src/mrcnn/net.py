import tensorflow as tf
import tensorflow.contrib.slim as slim

from fovea.backbone_net import YoloV3, Resnet
from fovea.net import FPN
from yolact.net import PredictHead
from fovea.tf_utils import FoveaCoder, get_batch_nms
from fovea.model import flat

class MaskRCNN():
    def __init__(self, n_class, n_feature, strides, normalize_factors,  scale_ranges,  
                 pos_shrink = 0.3, neg_shrink=0.4, roi_pool = 14, roi_sample_ratio = 4, 
                 nms_max_n=10, nms_threshold=0.5, obj_threshold=0.5, n_class_feature= None, 
                 network = 'Resnet', **backbone_args):
                 
        if network in ('Resnet', 'resnet'):
            self.backbone = Resnet(scope='backbone', **backbone_args)
        else:
            self.backbone = YoloV3(scope='backbone', **backbone_args)
        self.n_class = n_class
        self.n_feature = n_feature
        self.fpn = FPN(n_feature, n_downsample_layer = 2, scope='FPN')
        self.predhead = PredictHead(n_class, n_feature, n_mask_coef = None, scope='Prediction')
        self.maskhead = MaskHead(n_class, n_feature, scope='maskpred')
        if n_class_feature is not None:
            self.classhead  = ClassHead(n_class, n_class_feature, scope='bboxpred')
        else:
            self.classhead = None

        self.coder_args = dict(strides = strides , normalize_factors = normalize_factors,  
                                scale_ranges = scale_ranges, n_class = n_class, 
                                pos_shrink  = pos_shrink, neg_shrink = neg_shrink)
        self.nms_max_n = nms_max_n
        self.nms_thresh= nms_threshold
        self.obj_thresh = obj_threshold
        self.roi_pool = roi_pool
        self.roi_sample_r = roi_sample_ratio

    def forward(self, x, is_training):
        H, W  = x.shape.as_list()[1:3]
        batch_size = tf.shape(x)[0]
        out = self.backbone.forward(x, is_training)
        fpn_out = self.fpn.forward(out[-3:])
        prediction = self.predhead.forward(fpn_out) #[?, conv_h, conv_w, 4 + n_class]

        # decode and nms
        self.conv_dim = [c.shape.as_list()[1:3] for c in fpn_out]
        self.coder = FoveaCoder(self.conv_dim, **self.coder_args)
        decode_bbox = self.coder.decode(prediction)
        pred_bbox = flat(decode_bbox) # [batch_size, n_box, 4]
        prediction = flat(prediction) #[batch_size, n_box, 4+n_class]
        pred_class = tf.sigmoid(prediction[..., 4:4+self.n_class])
        pred_obj = tf.reduce_max(pred_class, -1)
        nms = get_batch_nms(pred_bbox, pred_obj, self.nms_max_n, self.nms_thresh, self.obj_thresh)  #[batch_size, nms_max_n, 2(batch_id, box_idx)]
        nms = tf.stop_gradient(nms)
        nms_mask = tf.where(nms[..., 1] >0, tf.ones_like(nms[..., 1]), tf.zeros_like(nms[..., 1]))[..., None] #[batch_size, nms_max_n, 1]
        nms *= nms_mask 
        
        #  nms_pred_mask
        roi_feature = RoiAlign([self.roi_pool, self.roi_pool], self.roi_sample_r)(
            fpn_out, tf.stop_gradient(pred_bbox), nms, H, W) #[batch_size, n_box, pool_h, pool_w, c]
        self.roi_feature_mask = tf.reshape(roi_feature, [-1, self.roi_pool, self.roi_pool,  self.n_feature])
        nms_pred_mask_logit = self.maskhead.forward(self.roi_feature_mask, is_training) # [?, roi_pool*2, roi_pool*2, n_class]
        nms_pred_mask_logit = tf.reshape(nms_pred_mask_logit, [batch_size, -1, self.roi_pool*2, self.roi_pool*2, self.n_class])
        nms_pred_class = tf.gather_nd(pred_class, nms) * tf.cast(nms_mask, tf.float32)
        nms_pred_bbox = tf.cast(tf.gather_nd(tf.stop_gradient(pred_bbox), nms), tf.int32) * nms_mask
        nms_pred_mask_logit= tf.map_fn(lambda b: 
                            tf.map_fn(lambda i:
                                resize_mask_to_bounding_box(
                                    nms_pred_mask_logit[tf.cast(b, tf.int32), tf.cast(i, tf.int32)], 
                                    nms_pred_bbox[tf.cast(b, tf.int32), tf.cast(i, tf.int32)], H, W), 
                            tf.cast(tf.range(self.nms_max_n), tf.float32)),
                        tf.cast(tf.range(batch_size), tf.float32))
        nms_pred_mask = tf.cast(tf.sigmoid(nms_pred_mask_logit) >0.5, tf.float32)
        
        # nms_pred_bbox
        if self.classhead  is None:
            nms_pred_logit = self.roi_feature_class  = None
        else:
            roi_feature = RoiAlign([self.roi_pool//2, self.roi_pool//2], self.roi_sample_r)(
                fpn_out, tf.stop_gradient(pred_bbox), tf.stop_gradient(nms), H, W) #[batch_size, n_nms, pool_h/2, pool_w/2, c]
            self.roi_feature_class = tf.reshape(roi_feature, [-1, self.roi_pool//2, self.roi_pool//2,  self.n_feature])
            nms_pred_logit = self.classhead.forward(self.roi_feature_class, is_training)  #[-1, n_class+1]
            nms_pred_logit = tf.reshape(nms_pred_logit, [batch_size, -1, self.n_class +1])
            nms_pred_class = tf.sigmoid(nms_pred_logit[..., 0:-1])* tf.cast(nms_mask, tf.float32)
             
        return prediction, pred_bbox, nms, nms_mask, nms_pred_mask_logit, nms_pred_mask, nms_pred_class, nms_pred_logit
        
        
class MaskHead():
    def __init__(self, n_class, n_feature= 256, scope='mask'):
        self.n_class = n_class
        self.n_feature = n_feature
        self.scope = scope

    def forward(self, x, is_training = True):
        # x: [?, pool_h, pool_w, C]
        with tf.variable_scope(self.scope):
            x_mask=slim.repeat(x, 4, slim.conv2d, self.n_feature, [3, 3],  scope='conv_feature_mask', activation_fn=tf.nn.relu, 
                            normalizer_fn=tf.layers.batch_normalization,
                            normalizer_params={'training': is_training})   
            x_mask =  slim.conv2d_transpose(x_mask, self.n_feature, 
                                            [2, 2], stride =2,  scope='conv2d_mask', activation_fn = tf.nn.relu)
            x_mask = slim.conv2d(x_mask, self.n_class, (1, 1), scope='conv_mask_class', activation_fn = None)
        return x_mask
        
class ClassHead():
    def __init__(self, n_class, n_feature= 1024, scope='bbox'):
        self.n_class = n_class
        self.n_feature = n_feature
        self.scope = scope

    def forward(self, x, is_training = True):
        # x: [?, pool_h, pool_w, C]
        pool_shape = x.shape.as_list()[1:3]
        with tf.variable_scope(self.scope):
            with slim.arg_scope([slim.conv2d], activation_fn=tf.nn.relu, 
                            normalizer_fn=tf.layers.batch_normalization,
                            normalizer_params={'training': is_training}, padding ='VALID'):
                x_class = slim.conv2d(x, self.n_feature, pool_shape, scope='conv_roi1')
                x_class = slim.conv2d(x_class, self.n_feature, [1, 1], scope='conv_roi2')
                x_class = x_class[:, 0, 0]           
            x_class = slim.fully_connected(x_class, self.n_class+1, scope='conv_bbox', activation_fn = None)
        return x_class #[?, self.n_class+1]
       
        
        

        
class RoiAlign():
    def __init__(self, pool_shape=[7, 7], sample_ratio=4):
        self.pool_shape  = pool_shape
        self.crop_size = [p*sample_ratio for p in pool_shape]
        self.sample_ratio  = sample_ratio

    def __call__(self, feature_list, unormalized_bbox, select_idx, H, W):
        '''
        Args:
            - feature_list: list of tf.float32, [][batch_size, conv_h, conv_w, c]
                - flat_feature: tf.concat([tf.reshape(batch_size, -1, c) for f in feature_list]), [batch_size, n_all_box, c]
            - unormalized_bbox: tf.float32, [batch_size, n_all_box, 4]
            - select_idx: tf.int32, [batch_size, n_box, 2(batch_idx, box_idx)]
            - H, W: origin size of image
        Returns:
            - [batch_size, n_box, pool_h, pool_w, c]
        '''
        boxes = tf.gather_nd(unormalized_bbox, select_idx)
        x1, y1, x2, y2 = tf.split(tf.clip_by_value(boxes/(W, H, W, H), 0, 1), 4, -1)
        normalized_boxes = tf.reshape(tf.concat([y1, x1, y2, x2], -1), [-1, 4])        
        batch_size, n_box =tf.shape(boxes)[0], tf.shape(boxes)[1]
        box_idx = tf.ones([n_box], dtype=tf.int32)[None] * tf.range(batch_size)[:, None] 

        pools = []
        feature_dim = 0
        for feature in feature_list:
            upper = feature.shape.as_list()[1] *  feature.shape.as_list()[2]
            select_mask = tf.math.logical_and(select_idx[..., 1] >= feature_dim, select_idx[..., 1] < feature_dim + upper) #[batch_size, n_box]
            feature_dim += upper

            unpool_feature = tf.image.crop_and_resize(feature, normalized_boxes, 
                                        tf.reshape(box_idx, [-1]), self.crop_size)
            pooled = slim.avg_pool2d(unpool_feature, self.sample_ratio, self.sample_ratio, 
                                     padding ='VALID') #[batch_size *n_box, pool_h, pool_w, c]
            pooled = tf.reshape(pooled, [batch_size, n_box] + self.pool_shape + [-1]) * tf.cast(select_mask, tf.float32)[..., None, None, None]
            pools.append(pooled[None]) #[1, batch_size *n_box, pool_h, pool_w, c]

        pools = tf.reduce_sum(tf.concat(pools, 0), 0)
        return pools

    
    
def resize_mask_to_bounding_box(mask, bbox, h, w):
    '''
    Args:
        - mask: tf.float32, [mask_h, mask_w, dim]
        - bbox : unnormalized bbox, tf.int32, [4(xmin, ymin, xmax, ymax)]
        - h, w: int or tf.int32
    Returns:
        - tf.float, [h, w, dim]
    '''
    xmin, ymin, xmax, ymax = bbox[0], bbox[1], bbox[2], bbox[3]
    xmin = tf.clip_by_value(xmin, 0, w)
    ymin = tf.clip_by_value(ymin, 0, h)
    xmax = tf.clip_by_value(xmax, 0, w)
    ymax = tf.clip_by_value(ymax, 0, h)
    mask_w = tf.clip_by_value(xmax - xmin, 1, w)
    mask_h = tf.clip_by_value(ymax - ymin, 1, h)    
    mask = tf.image.pad_to_bounding_box(tf.image.resize_bilinear(mask[None], [mask_h, mask_w]), ymin, xmin, h, w)[0]
    return mask




