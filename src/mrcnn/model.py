import tensorflow as tf
import numpy as np

from yolact import model as M
from .loss import Loss
from .net import MaskRCNN


class ModelBase(M.ModelBase):
    def __init__(self, config, img_size, n_class, reset_graph= True, **net_args):    
        M.ModelBase.__init__(self, config, img_size, n_class, reset_graph, **net_args)
                
    def define_network(self, **net_args):
        self.net = MaskRCNN(self.n_class, **net_args)
        self.pred, self.pred_bbox, self.nms, self.nms_mask, self.nms_pred_mask_logit, \
            self.nms_pred_mask, self.nms_pred_class, self.nms_pred_logit = self.net.forward(self.img, self.is_training)
        self.coder = self.net.coder
                    
    def get_pred_mask_value(self, pred_mask, pred_class):
        pred_mask = [[ m[..., np.argmax(p)]*(np.argmax(p)+1) for m, p in zip(mask, p_class)  if np.max(p)>0 
                ] for mask, p_class in zip(pred_mask, pred_class)]    
        return pred_mask
        
                
class ModelTrain(ModelBase):
    def __init__(self, base_args, data_args, loss_args, opt_args):
        ModelBase.__init__(self, **base_args)
        M.ModelTrain.define_dataset(self, **data_args)
        self.define_loss(**loss_args)
        M.ModelTrain.define_optimizer(self, **opt_args)       
        
    def define_loss(self, focal_alpha = 0.25, focal_gamma=2, smooth_l1_beta=0.11, iou_threshold=0.5):
        with self.graph.as_default():
            self.define_target()
            loss_fn = Loss(focal_alpha, focal_gamma, smooth_l1_beta, iou_threshold,
                            self.net.obj_thresh, self.net.nms_thresh, self.net.nms_max_n)
            with tf.name_scope('loss'):
                self.loss_dict = loss_fn(self.target, self.pred, self.instance, self.nms_pred_mask_logit,
                                         self.true_bbox, self.pred_bbox, self.nms, self.nms_mask, self.nms_pred_logit)  
    
    def one_epoch(self, *args):
        return M.ModelTrain.one_epoch(self, *args)
        
    def train(self, **kwargs):
        M.ModelTrain.train(self, **kwargs)
    