import os
from datetime import datetime, timedelta
import time
import numpy as np
import pylab as plt


class Log(object):
    def __init__(self, var_names, save_path, valid_ave_n = None):
        '''
        Args: 
            var_names: list, eg: ['loss', 'accuracy']    
            valid_ave_n: None or int
        '''
        os.makedirs(save_path, exist_ok=True)
        self.save_path = save_path
        self.var_names = var_names
        self.valid_ave_n = valid_ave_n
        self.evaluation = {}
        if self.valid_ave_n is None:
            for v in var_names:
                self.evaluation.update({"train_" + v :[], "valid_" + v:[]})
        else:
            for v in var_names:
                self.evaluation.update({"train_" + v :[], "valid_" + v:[], "ave_valid_" + v:[]})

        self.initial_log()
        
    def update_value(self, epoch, var_name, train_value, valid_value):
        self.evaluation["train_" + var_name].append(train_value)
        self.evaluation["valid_" + var_name].append(valid_value)
        if self.valid_ave_n is not None:
            ave_start = max(0, epoch - self.valid_ave_n)
            ave_end = epoch + 1
            self.evaluation["ave_valid_" + var_name].append(np.array(self.evaluation["valid_" + var_name])[ave_start: ave_end].mean())

    def current_time(self):
        return  (datetime.now() + \
                                 timedelta(hours =  9 + time.timezone / 60 / 60)).strftime('%Y%m%d_%H%M%S')

    def initial_log(self):
        self.log_file = open(self.save_path + '/log.txt','w+')
        self.log_file.write('starting time: ' + self.current_time() + '\r\n')
        header = 'epoch, '
        for v in self.var_names:
            header += "train_" + v + ', '
            header += "valid_" + v + ', '
            if self.valid_ave_n is not None:
                header += "ave_valid_" + v + ', '
        header = header + 'time \n'
        self.log_file.write(header)
        self.log_file.close()

    def update_log(self, epoch):
        self.log_file = open(self.save_path + '/log.txt','a+')
        header = str(epoch) + ', '
        for var_name in self.var_names:
            max_i = len(self.evaluation["train_" + var_name])
            header +=  '%8.5f' % (self.evaluation["train_" + var_name][max_i - 1]) + ', '
            header +=  '%8.5f' % (self.evaluation["valid_" + var_name][max_i - 1]) + ', '
            if self.valid_ave_n is not None:
                header +=  '%8.5f' % (self.evaluation["ave_valid_" + var_name][max_i - 1]) + ', '
        header +=  self.current_time() + ' \n'
        self.log_file.write(header)
        self.log_file.close()

    def update_plot(self, show_fig_start = 0):
        for var in self.var_names:
            fig = plt.figure(figsize=(8, 5))
            plt.plot(self.evaluation['train_' + var][show_fig_start:], label='train')
            if self.valid_ave_n is not None:
                plt.plot(self.evaluation["ave_valid_" + var][show_fig_start:], label='ave_valid')
            else:
                plt.plot(self.evaluation["valid_" + var][show_fig_start:], label='valid')
            plt.title(var)
            plt.legend()
            plt.show()
            fig.savefig(self.save_path + '/' + var + '.png')
        plt.close('all')