import tensorflow as tf


def random_pad(img, img_size, reshape_size, random=True):
    if random:
        offset = tf.cast(tf.map_fn(lambda i: tf.maximum(tf.random_uniform([], 0, 1) *  i, tf.zeros([])),
                        tf.cast((img_size - reshape_size)//2, tf.float32)), tf.int32)
    else:
        offset =tf.cast((img_size-reshape_size)//2, tf.int32)
    padding = tf.stack([
            offset[0],  
            tf.maximum(tf.zeros([], dtype=tf.int32), (tf.cast(img_size- reshape_size, tf.int32) -offset)[0]), 
            offset[1],   
            tf.maximum(tf.zeros([], dtype=tf.int32), (tf.cast(img_size- reshape_size, tf.int32) -offset)[1]),
            tf.zeros([], dtype=tf.int32), 
            tf.zeros([], dtype=tf.int32)
    ])
    aug_img = tf.pad(img, tf.reshape(padding, [3, 2]), constant_values=0)
    offset_h, offset_w = padding[2], padding[0]
    return aug_img, offset_h, offset_w

def random_crop(img, img_size, reshape_size):
    offset=  tf.cast(tf.random_uniform([], 0, 1) * (reshape_size - img_size)/2, tf.int32)
    aug_img = img[offset[0]:offset[0]+img_size[0], offset[1]:offset[1]+img_size[1]]
    offset_h, offset_w = offset[1], offset[0]
    return aug_img, offset_h, offset_w
        
def resize_with_pad_and_ratio(tf_img, img_size, resize_ratio = 1, random = False):
    '''
    Args: 
        - tf_img: tf.float32, [H, W, C]
    '''
    origin_size = tf.cast(tf.shape(tf_img), dtype=tf.float32)[0:2]
    resize_factor = tf.reduce_min(img_size /origin_size, 0) * resize_ratio
    resize_shape = tf.cast(resize_factor * origin_size, tf.int32)
    aug_img =  tf.image.resize_images(tf_img, resize_shape,
                                    method=tf.image.ResizeMethod.NEAREST_NEIGHBOR, 
                                    align_corners= True)
    aug_size = tf.cast(tf.shape(aug_img), dtype=tf.float32)[0:2]
    aug_img, offset_h, offset_w = random_pad(aug_img, img_size, aug_size, random = random) 
    return aug_img
    
    
def flip_left_right(img, box = None):
    aug_img = img[:, ::-1]
    if box is None:
        return aug_img
    else:
        aug_box = tf.abs(box - tf.cast(tf.stack([tf.shape(img)[1], 0,  tf.shape(img)[1], 0]), tf.float32))
        aug_box = tf.concat((aug_box[:, 2:3], aug_box[:, 1:2], aug_box[:, 0:1], aug_box[:, 3:4]), 1)
        return aug_img, aug_box
        