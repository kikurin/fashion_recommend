import math
        
class EarlyStopper():
    def __init__(self, patiences=None):
        self.best_loss = math.inf
        self.last_improve = 0
        self.stop = False
        self.patiences = patiences

    def __call__(self, epoch, loss_now):
        if loss_now < self.best_loss:
            self.best_loss = loss_now
            self.last_improve = epoch
            self.improve=True
        else:
            self.improve = False
        if self.patiences is not None and epoch-self.last_improve > self.patiences:
            self.stop=True
        return self.improve, self.stop