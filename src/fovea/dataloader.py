import tensorflow as tf
import numpy as np
import json
import math
import itertools

from utils.img_augmentation import random_pad, random_crop, flip_left_right
from deeprank.dataloader import img_folder_to_json, img_list_to_json


class DataLoader():
    def __init__(self, filename, img_size, class_dict, batch_size, buffer_size, num_threads, 
                n_max_box, with_label, data_aug, no_obj_sampling_w=None):
        self.img_size= img_size #[h, w]
        self.data_aug = data_aug
        self.with_label= with_label
        self.n_max_box = n_max_box

        try:
            json_data = json.load(open(filename))
            if self.with_label:
                self.box_idx = np.array([0] + [len(json_data[k]['xmins']) for k in json_data.keys()]).cumsum()
                self.box_idx = tf.convert_to_tensor(self.box_idx)
                self.bbox = []
                for k in json_data.keys():
                    bbox = json_data[k]['xmins'] + json_data[k]['ymins'] + json_data[k]['xmaxs'] +json_data[k]['ymaxs']+[ class_dict.get(c, -1) for c in json_data[k]['labels']]
                    self.bbox += np.array(bbox).astype(np.int32).reshape(5, -1).transpose().tolist()
                self.bbox = tf.convert_to_tensor(self.bbox)
        except:
            try: 
                json_data = img_folder_to_json(filename)
            except:
                json_data = img_list_to_json(filename)
        self.img_paths = tf.convert_to_tensor([json_data[s]['img_path'] for s in json_data.keys()])


        if no_obj_sampling_w is None:
            self.dataset = tf.data.Dataset.from_tensor_slices(np.arange(0, len(json_data)) )
        else:
            n_obj = np.where( np.array([len(json_data[k]['xmins']) for k in json_data.keys()]) > 0, 1, 0).sum()
            n_nobj = len(json_data) - n_obj
            w_nobj  = n_obj * no_obj_sampling_w / n_nobj
            self.weight =[0] + [1 if json_data[k]['xmins']!=[] else w_nobj for k in json_data.keys() ]
            self.weight = np.cumsum(self.weight)
            self.dataset = tf.data.Dataset.from_generator(self.generator, (tf.int64), (tf.TensorShape([])))

        self.dataset = self.dataset.map(lambda x: self.__next__(x), 
                                    num_parallel_calls=num_threads).prefetch(1)
        if buffer_size <= 0:
            self.dataset = self.dataset.batch(batch_size)
        else:
            self.dataset = self.dataset.shuffle(buffer_size, reshuffle_each_iteration= True).batch(batch_size)
        self.iterator = self.dataset.make_initializable_iterator()
        self.next_op = self.iterator.get_next()
        self.max_iter = math.ceil(len(json_data)/batch_size)

    def generator(self):
        for i in itertools.count(1):
            random_p = np.random.uniform(0, self.weight[-1])
            idx = np.where(self.weight<=random_p)[0][-1]
            yield idx 

    def initialize(self, sess):
        sess.run(self.iterator.initializer)

    def get_gtbox(self, box, label):
        '''
        Args: 
            - box: tf.float32, [?, 4(xmin, ymin, xmax, ymax)]
            - label: tf.int32, [?, 1]
        Returns:
            - gt_box: [self.n_max_box, 5(xmin, ymin, xmax, ymax, label)]
                - if no objects, all values will be -1
        '''

        box_size = tf.reduce_prod(box[:, 2:4] - box[:, 0:2], 1)
        box_idx = tf.where(box_size>25)[0: self.n_max_box, 0]
        box = tf.gather(box, box_idx)
        label = tf.gather(label, box_idx)
        gt_box = tf.concat([tf.cast(box, tf.int32), label], -1)
        gt_box = tf.concat([gt_box, tf.zeros([self.n_max_box - tf.shape(gt_box)[0], 5], dtype=tf.int32)-1], 0)
        return gt_box

    def __next__(self, idx):
        img_name = self.img_paths[idx]
        tf_img =  tf.image.decode_png(tf.read_file(img_name), channels=3)
        if self.with_label:
            bbox = self.bbox[self.box_idx[idx]: self.box_idx[idx+1]]
        else:
            bbox = tf.ones([1, 5])
        box, label = tf.cast(bbox[:, 0:4], tf.float32), bbox[:, 4:5]

        origin_size = tf.cast(tf.shape(tf_img), dtype=tf.float32)[0:2]
        resize_ratio =  tf.random_uniform([], 0.75, 1.5) if self.data_aug else 1
        resize_factor = tf.reduce_min(self.img_size /origin_size, 0) * resize_ratio
        resize_shape = tf.cast(resize_factor * origin_size, tf.int32)
        aug_img =  tf.image.resize_images(tf_img, resize_shape,
                                        method=tf.image.ResizeMethod.NEAREST_NEIGHBOR, 
                                        align_corners= True)
        aug_box = box*resize_factor

        aug_size = tf.cast(tf.shape(aug_img), dtype=tf.float32)[0:2]
        if self.data_aug: 
            # random_pad
            aug_img, offset_h, offset_w = tf.cond(
                tf.reduce_min(aug_size/self.img_size)<1, 
                lambda: random_pad(aug_img, self.img_size, aug_size), 
                lambda: (aug_img, 0, 0)
            )    
            aug_box += tf.cast(tf.stack([offset_h, offset_w, offset_h, offset_w]), tf.float32)
            # random_crop
            aug_size = tf.cast(tf.shape(aug_img), dtype=tf.float32)[0:2]
            aug_img, offset_h, offset_w = random_crop(aug_img, self.img_size, aug_size)
            aug_box -= tf.cast(tf.stack([offset_h, offset_w, offset_h, offset_w]), tf.float32)

            # adjust coordinate 
            h, w = self.img_size
            coord_max = [w, h, w, h]
            aug_box = tf.minimum(tf.maximum(aug_box/coord_max, 0), 1)*coord_max

            aug_img, aug_box = tf.cond(tf.random_uniform([], 0, 1) > 0.5,
                lambda: flip_left_right(aug_img, aug_box),
                lambda: (aug_img, aug_box))

        else:
            aug_img, offset_h, offset_w = random_pad(aug_img, self.img_size, aug_size, random = False) 
            aug_box += tf.cast(tf.stack([offset_h, offset_w, offset_h, offset_w]), tf.float32)

        img, box = aug_img, aug_box
        mean = tf.constant([0.485, 0.456, 0.406], dtype = tf.float32)
        std = tf.constant([0.229, 0.224, 0.225], dtype=tf.float32)
        img = (img/255 - mean)/std
        if self.with_label:
            gt_box = self.get_gtbox(box, label)
            return img, gt_box
        else:
            return img, self.img_paths[idx]

