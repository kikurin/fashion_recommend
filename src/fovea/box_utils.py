import numpy as np
import cv2
import pylab as plt

def draw_box(img, box_coord_list, with_img=False, label_list=None):
    '''
    img : [h, w, 3]
    box_coord_list[i]: [xmin, ymin, xmax, ymax]
    '''
    img0 =img.copy()[..., 0:3]
    line_w = int(max(img0.shape)/200)
    color_i = int(np.round(img.max()))
    if label_list is None:
        label_list = np.repeat('', len(box_coord_list))
    for b, l in zip(box_coord_list, label_list):
        xmin, ymin, xmax, ymax = [int(i) for i in b[0:4]]
        img0 = cv2.rectangle(img0, (xmin, ymin), (xmax, ymax), (color_i, 0, 0), line_w)
        if label_list is not None:
            img0 = cv2.putText(img0, str(l), (xmin, ymin-10),  cv2.FONT_HERSHEY_COMPLEX, line_w/5, (0, color_i, 0), 2)
    plt.imshow(img0)
    
    if with_img:
        return img0
    
def area_size(box):
    xmin, ymin, xmax, ymax = box   
    return (ymax - ymin) * (xmax - xmin)

def intersection_area(box1, box2):
    xmin1, ymin1, xmax1, ymax1 = box1
    xmin2, ymin2, xmax2, ymax2 = box2
    overlap_xmin = max(xmin1, xmin2)
    overlap_ymin = max(ymin1, ymin2)
    overlap_xmax = min(xmax1, xmax2)
    overlap_ymax = min(ymax1, ymax2)

    overlap_w = max(0, overlap_xmax - overlap_xmin)
    overlap_h = max(0, overlap_ymax - overlap_ymin)
    return overlap_w * overlap_h

def iou(box1, box2):
    intersect = intersection_area(box1, box2)
    union = area_size(box1) + area_size(box2) - intersect + 1e-6
    return intersect / union

def evaluate_mAP(pred_box_lists, true_box_lists, n_class, iou_thresh = 0.5):
    '''
    Args:
        - pred_box_lists[i]: [[xmin, xmax, ymin, ymax, conf, class_p0, ...], [...], [...], ...]
        - true_box_lists[i]: [[xmin, xmax, ymin, ymax, class], [...], [...], ...]
        - iou_thresh: float between [0, 1].
            - Correct if predicted box share more than ```iou_thresh``` with ground truth box
    Ouput:
        mAP: float. Mean average precision
    '''
    
    batch_size = len(pred_box_lists)
    AP =[]
    # TP: IOU> threshold and pred_class is correct
    # FP: IOU<0.5, and pred_class is correct
    for c in range(n_class):
        pred_matches, pred_confs, trues = [], [], []
        for i in range(batch_size):
            if len(true_box_lists[i])>0:
                true_box = [bb[0:4] for bb in true_box_lists[i] if bb[4]==c]
                pred_box = [bb[0:5] for bb in pred_box_lists[i] if np.argmax(bb[5:])==c]
    
                pred_match = np.zeros(len(pred_box))
                pred_conf = [p[4] for p in pred_box]
                true_match = np.zeros(len(true_box))       
                for p in range(len(pred_match)):
                    ious = [iou(t, pred_box[p][0:4]) for t in true_box]
                    sorted_ixs =np.argsort(-np.array(ious))
                    for t in sorted_ixs:
                        if true_match[t]==1: # skip gt_box if it's already detected
                            continue 
                        if ious[t] < iou_thresh:
                            break               
                        true_match[t] = 1
                        pred_match[p] = 1       
                pred_matches = np.append(pred_matches, pred_match)
                pred_confs = np.append(pred_confs, pred_conf)
                trues = np.append(trues, true_match)    
                
        if len(trues)>0:   
            # recall-precision
            idx = np.argsort(-pred_confs)
            preds = pred_matches[idx]
            precisions = np.cumsum(preds) / (np.arange(len(preds)) + 1)
            precisions = np.concatenate([[0], precisions, [0]])
            for i in range(len(precisions) - 2, -1, -1): # ensure precisions is decreasing
                precisions[i] = np.maximum(precisions[i], precisions[i + 1])
            recalls = np.cumsum(preds) / len(trues)
            recalls = np.concatenate([[0], recalls, [1]])
    
            max_precision = []
            for r in np.unique(recalls):
                max_prec = np.max(precisions[ np.where(recalls>=r)])
                max_precision.append([r, max_prec])
    
            average_precision = 0 
            for i in range(1, len(max_precision)):
                average_precision += (max_precision[i][0] - max_precision[i-1][0]) * max_precision[i][1]
            AP.append(average_precision)
    mAP = np.mean(AP)
    return mAP
