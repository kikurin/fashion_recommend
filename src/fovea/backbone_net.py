import tensorflow as tf
import tensorflow.contrib.slim as slim
import re
import pickle
import numpy as np

class Backbone():
    def __init__(self, scope):
        self.scope = scope
        
    def load_pkl(self, sess, load_pkl):
        tf_vars = tf.trainable_variables(scope= self.scope)
        moving_vars = [v for v in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES) if 
                       re.findall('%s(.*)moving'%self.scope, v.name)!=[]]
        with open(load_pkl, mode='rb') as f:
            vars = pickle.load(f)
        var_name = list(vars.keys())
        from_name = [v for v in var_name if re.findall('um_batches_tracked', v)==[] and re.findall('fc', v)==[]] 

        assign_op = []
        i, j = 0, 0
        for v in from_name:
            if i==len(tf_vars):
                break
            from_shape = list(vars[v].shape) 
            var_now  =vars[v]
            if len(from_shape)==4:
                var_now = np.transpose(var_now, [2, 3, 1, 0])
            if re.findall('running', v)==[]:
                assign_op.append(tf.assign(tf_vars[i], var_now))
                i +=1
            else:
                assign_op.append(tf.assign(moving_vars[j], var_now))
                j +=1
        _ = sess.run(assign_op)

        
class Resnet(Backbone):
    def __init__(self, layer = [3, 4, 6, 3], scope='Resnet'):
        Backbone.__init__(self, scope)
        self.layer = layer

    def forward(self, x, is_training = True):
        # x: [None, H, W, C]
        with tf.variable_scope(self.scope):
            h1 = slim.conv2d(x, 64, [7, 7], stride = 2, scope='res1',     
                                activation_fn=None, biases_initializer = None, 
                                normalizer_fn=tf.layers.batch_normalization,
                                normalizer_params={'training': is_training, 'renorm': True})
            h2 =  res_layer(h1, block_n= self.layer[0], out_ch_list=[64, 256], 
                            init_stride=2, name='res2', is_training=is_training)
            h3 = res_layer(h2, block_n = self.layer[1], out_ch_list = [128, 512], 
                           init_stride=2, name='res3', is_training=is_training)
            h4 = res_layer(h3, block_n = self.layer[2], out_ch_list = [256, 1024], 
                           init_stride=2, name='res4', is_training=is_training)
            h5 = res_layer(h4, block_n =self.layer[3], out_ch_list = [512, 2048], 
                           init_stride=2, name='res5', is_training=is_training)
            out = [h1, h2, h3, h4, h5]
        return out

def res_block(x, out_ch_list, block_stride, change_dimension = True, name = 'res', is_training = False, dilation=1):
    with tf.variable_scope(name): 
        with slim.arg_scope([slim.conv2d],
                        rate = 1, 
                        activation_fn = None, 
                        biases_initializer = None, 
                        normalizer_fn=tf.layers.batch_normalization,
                        normalizer_params={'training': is_training, 'renorm': True}):
            h= tf.nn.relu(slim.conv2d(x, out_ch_list[0], [1, 1], stride = 1, scope='conv1'))
            h = tf.nn.relu(slim.conv2d(h, out_ch_list[0], [3, 3], stride =block_stride,  rate = dilation, scope='conv2'))
            h = slim.conv2d(h, out_ch_list[1], [1, 1], stride = 1, scope='conv3')
            if change_dimension:
                x = slim.conv2d(x, out_ch_list[1], [1, 1], stride = block_stride, scope='conv4')
            out = tf.nn.relu(x + h )
    return out

def res_layer(x, block_n, out_ch_list, init_stride = 2, name='res_layer', is_training=False, dilation=1):
    h = x
    with tf.variable_scope(name): 
        for i in range(block_n):
            if i ==0:
                h = res_block(h, out_ch_list, init_stride, change_dimension=True, name='block_%s' %i, 
                                            is_training =is_training)
            else:
                h = res_block(h, out_ch_list,  1, change_dimension=False, name='block_%s' %i, 
                                            is_training =is_training, dilation=dilation)
    return h

class YoloV3(Backbone):
    def __init__(self, scope='Yolo3', activation = lambda x: tf.nn.leaky_relu(x, alpha=0.1)):
        Backbone.__init__(self, scope)
        self.activation = activation

    def forward(self, x, is_training):
        activation_fn = self.activation
        with tf.variable_scope(self.scope):
            h = conv2d_fixed_padding(x, 32, 3, 1, activation_fn, is_training, 'h1/conv1')
            h = conv2d_fixed_padding(h, 64, 3, 2, activation_fn, is_training, 'h1/conv2')
            h1 =  darknet53_block(h, 1, activation_fn, is_training, 'h1')

            h2 = conv2d_fixed_padding(h1, 128, 3, 2, activation_fn, is_training, 'h2/conv')
            h2 =  darknet53_block(h2, 2, activation_fn, is_training, 'h2')

            h3 = conv2d_fixed_padding(h2, 256, 3, 2, activation_fn, is_training, 'h3/conv')
            h3 =  darknet53_block(h3, 8, activation_fn, is_training, 'h3')

            h4 = conv2d_fixed_padding(h3, 512, 3, 2, activation_fn, is_training, 'h4/conv')
            h4 =  darknet53_block(h4, 8, activation_fn, is_training, 'h4')

            h5 = conv2d_fixed_padding(h4, 1024, 3, 2, activation_fn, is_training, 'h5/conv')
            h5 =  darknet53_block(h5, 4, activation_fn, is_training, 'h5')
        out = [h1, h2, h3, h4, h5]
        return out

        
def conv2d_fixed_padding(x, out_ch, ksize, stride, activation_fn = tf.nn.relu, is_training = True, scope='conv'):
    with slim.arg_scope([slim.conv2d],
                    activation_fn = None, 
                    biases_initializer = None, 
                    normalizer_fn=tf.layers.batch_normalization,
                    normalizer_params={'training': is_training}):
        h = slim.conv2d(x, out_ch, ksize, stride, scope=scope)
        h = activation_fn(h)
    return h

def res_conv(x, activation_fn, is_training, scope):
    with tf.variable_scope(scope):
        in_ch = x.shape.as_list()[-1]
        h = conv2d_fixed_padding(x, in_ch//2, 1, 1, activation_fn, is_training, 'conv1')
        h = conv2d_fixed_padding(h, in_ch, 3, 1, activation_fn, is_training, 'conv2')
    return h + x

def darknet53_block(x, n_layer, activation_fn, is_training, scope):
    h = x
    with tf.variable_scope(scope):
        for i in range(n_layer):
            h = res_conv(h, activation_fn, is_training, 'res_%d'%i)
    return h