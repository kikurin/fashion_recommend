import tensorflow as tf
import numpy as np


class FoveaCoder():
    def __init__(self, feature_dims, strides, normalize_factors, n_class, scale_ranges, pos_shrink = 0.3, neg_shrink=0.4):
        assert len(feature_dims) == len(strides), 'length of feature_dims and strides are unmatched'
        assert len(normalize_factors) == len(strides), 'length of normalize_factors and strides are unmatched'
        assert len(scale_ranges) == len(strides), 'length of scale_ranges and strides are unmatched'
        assert pos_shrink <= neg_shrink <= 1, 'inappropriate shrink ratios'

        self.feature_dims = feature_dims
        self.strides = strides 
        self.normalize_factors = normalize_factors
        self.scale_ranges = scale_ranges
        self.n_class = n_class
        self.pos_shrink = pos_shrink
        self.neg_shrink = neg_shrink

    def encode_target(self, gt_box, H, W, stride, lower_scale, upper_scale, z):
        '''
        Args:
            - gt_box: ground truth bbox, tf.float32 with shape [?, n_box, 5/6(xmin, ymin, xmax, ymax, label, instance)]
            - H, W: integer, feature map's dimension
            - stride: integer
            - lower_scale, upper_scale: scale range of sqrt(bbox's area)
        Returns: [?, H, W, 4+n_class]
            - offset_bbox: tf.float32, [?, H, W, 4(xmin, ymin, xmax, ymax)]
                - normalized bounding box's offset
            - fovea_class: binary class map. tf.float32,  [?, H, W, n_class]
                - 1: positive, 0: negative, -1: neutral
        '''
        n_dim = gt_box.shape.as_list()[-1]
        batch_size, n_box = tf.shape(gt_box)[0], tf.shape(gt_box)[1]

        mins, maxs = gt_box[..., 0:2], gt_box[..., 2:4]
        area = tf.sqrt(tf.reduce_prod(maxs-  mins, -1)) 
        area_mask = tf.cast(area>= lower_scale, tf.float32) * tf.cast(area<=upper_scale, tf.float32) #[batch_size, n_box]
        gt_box = tf.where(tf.tile(area_mask[..., None], [1, 1, n_dim]) > 0, gt_box, -tf.ones_like(gt_box))
        _, idx=  tf.nn.top_k(area * area_mask, n_box) #biggest: 0, [batch_size, n_box]
        idx1 = tf.ones_like(idx) * tf.range(batch_size)[:, None]
        idx1 = tf.concat([idx1[None], idx[None]], 0) #[2(batch_idx, box_idx), batch_size, n_box]
        gt_box= tf.gather_nd(gt_box, tf.transpose(idx1, [1, 2, 0])) 
        mins, maxs, labels = gt_box[..., 0:2], gt_box[..., 2:4], gt_box[..., 4]
        if n_dim==6:
            instances = gt_box[..., 5]

        # calculate fovea_positive, fovea_negative
        rmins, rmaxs = mins/stride, maxs/stride
        rcenters = rmins  + 0.5 * (rmaxs - rmins)
        pos_mins = rcenters - 0.5* (rmaxs - rmins) * self.pos_shrink
        pos_maxs =  rcenters + 0.5* (rmaxs - rmins) * self.pos_shrink
        neg_mins = rcenters - 0.5* (rmaxs - rmins) * self.neg_shrink
        neg_maxs =  rcenters + 0.5* (rmaxs - rmins) * self.neg_shrink
        grid_xy = get_meshgrid(H, W, tf.float32)

        def get_fovea_area(grid_xy, mins, maxs):
            grid_xy =grid_xy[None, None] #[1, 1, H, W, 2]
            mins = mins[:, :, None, None] #[batch_size, n_box, 1 ,1, 2]
            maxs = maxs[:, :, None, None]
            fovea = tf.cast(grid_xy >= mins, tf.float32) *  tf.cast(grid_xy <=maxs, tf.float32)
            return tf.reduce_prod(fovea, -1)
        pos_fovea = get_fovea_area(grid_xy, pos_mins, pos_maxs) #[batch_size, n_box, H, W]
        neg_fovea = get_fovea_area(grid_xy, neg_mins, neg_maxs)  #[batch_size, n_box, H, W]

        # inside pos: 1, outside neg: 0, inside neg but outside pos: -1
        labels = tf.cast(labels, tf.int32)
        foveas = 2*pos_fovea + (1-neg_fovea) - 1         
        def get_fovea_class(fovea, label):
            f_class = tf.dynamic_partition(tf.cast(fovea, tf.int32), label+1, self.n_class+1)[1:] # exclude label ==-1
            f_class_sum = tf.concat([tf.reduce_sum(f, 0)[..., None] for f in f_class], -1) #[H, W, n_class]
            return f_class_sum           
        fovea_class = tf.map_fn(lambda i: get_fovea_class(foveas[i], labels[i]), 
                                tf.cast(tf.range(batch_size), tf.int32)) #[batch_size, H, W, n_class]

        # only annotate offset inside neg_fovea
        offset_bbox = normalize_offset(gt_box[..., 0:4],  grid_xy, stride, z) #[H, W, batch_size, n_box, 4]
        offset_bbox = tf.transpose(offset_bbox, [2, 3, 0, 1, 4]) * neg_fovea[..., None] #[batch_size, n_box, H, W, 4]
        # if multiple objects is in the same feature map's cell, only keep the smallest one
        min_mask = tf.transpose(tf.cast(neg_fovea, tf.int32), [0, 2, 3, 1]) * -tf.range(n_box)
        min_mask = -tf.reduce_min(min_mask, -1) #[batch_size, H, W]
        min_mask= tf.one_hot(min_mask, n_box) #[batch_size, H, W, n_box]
        min_mask =tf.transpose(min_mask, [0, 3, 1, 2])[..., None] #[batch_size, n_box, H, W, 1]
        offset_bbox = tf.reduce_sum(offset_bbox * min_mask, 1) #[batch_size, H, W, 4]

        encoded = tf.concat([offset_bbox, tf.cast(fovea_class, tf.float32)], -1) #[batch_size, H, W, 4+n_class]
        if n_dim==6:
            instance_mask = neg_fovea * instances[..., None, None] #[batch_size, n_box, H, W]
            instance_mask= tf.reduce_sum(instance_mask * min_mask[..., 0], 1) #[batch_size, H, W]
            encoded = tf.concat((encoded, instance_mask[..., None]), -1) #[batch_size, H, W, 4+n_class +1]
        return  encoded

    def encode(self, gt_box):
        # Args: [?, n_box, 5(xmin, ymin, xmax, ymax, label)]
        # Returns: list, [scale][?, h, w, 4+n_class]
        batch = []
        for (H, W), stride, (lower_scale, upper_scale), z in zip(self.feature_dims, self.strides, self.scale_ranges,  self.normalize_factors):
            batch.append( self.encode_target(gt_box, H, W, stride, lower_scale, upper_scale, z) )
        return batch
        
    def decode(self, bbox):
        # bbox: list, [scale][?, h, w, 4]
        # Returns: list, [scale][?, h, w, 4]
        batch= []
        for b, (H, W), stride, z in zip(bbox, self.feature_dims, self.strides, self.normalize_factors):
            grid_xy = get_meshgrid(H, W, tf.float32)
            batch.append( denormalize_offset(b, grid_xy, stride, z) )
        return batch       
        
 
def get_meshgrid(H, W, dtype = tf.float32):
    x = tf.tile(tf.range(W)[None], [H, 1])[..., None]
    y = tf.tile(tf.range(H)[:, None], [1, W])[..., None]
    grid_xy = tf.concat([x, y], -1)#[H, W, 2(x, y)]
    return tf.cast(grid_xy, dtype)
    
def normalize_offset(bbox, grid_xy, stride, normalize_scale):
    ''' 
    Args: 
        - bbox: tf.float32, original coordinates [..., 4(xmin, ymin, xmax, ymax)]
        - grid_xy: tf.flaot32, indicies matrix,  [H, W, 2(x, y)]
        - stride, normalize_scale : integer
    Returns:
        -offset_bbox: [H, W, ...,  4(xmin, ymin, xmax, ymax)]
    '''
    mutual_dim = len(bbox.shape.as_list()) - 1
    mins, maxs = bbox[..., 0:2], bbox[..., 2:4]
    cell_xy = stride * (grid_xy + 0.5)
    for _  in range(mutual_dim):
        cell_xy = tf.expand_dims(cell_xy, -2)

    offset_mins = (cell_xy- mins)/normalize_scale
    offset_maxs = (maxs - cell_xy)/normalize_scale
    offset_bbox = tf.concat([offset_mins, offset_maxs], -1)
    offset_bbox = tf.log(tf.clip_by_value(offset_bbox, 1e-5, 1e5))
    return offset_bbox


def denormalize_offset(offset_bbox, grid_xy, stride, normalize_scale):
    mutual_dim = len(offset_bbox.shape.as_list()) - 3
    cell_xy = stride * (grid_xy + 0.5) #[H, W, 2]
    for _  in range(mutual_dim):
        cell_xy = tf.expand_dims(cell_xy, 0) #[..., H, W, 2]

    offset_bbox = tf.exp(offset_bbox) #[..., H, W, 4]
    tmins, tmaxs = offset_bbox[..., 0:2], offset_bbox[..., 2:4]
    mins = cell_xy - tmins * normalize_scale 
    maxs = cell_xy + tmaxs * normalize_scale 
    bbox = tf.concat([mins, maxs], -1)
    return bbox

    
# nms
    
def get_nms(pred_bbox, pred_obj, max_output, iou_threshold, score_threshold, idx = None):
    '''
    Args: 
        - pred_bbox: [?, 4]
        - pred_obj: [?]
    '''

    nms = tf.image.non_max_suppression(pred_bbox, pred_obj, max_output,  iou_threshold, score_threshold)
    filler = -tf.ones([max_output- tf.shape(nms)[0]], dtype=tf.int32)
    nms = tf.concat([nms, filler], 0)
    if idx is None:
        return nms #[max_output]
    else:
        filler= tf.ones_like(nms) * idx
        return tf.concat([filler[:, None], nms[:, None]], -1) #[max_output, 2(idx, nms)]

def get_batch_nms(pred_bbox, pred_obj, max_output, iou_threshold, score_threshold):
    '''
    Args: 
        - pred_bbox: [batch_size, ?, 4]
        - pred_obj: [batch_size, ?]
    '''
    batch_size = tf.shape(pred_bbox)[0]
    nms = tf.map_fn(lambda i: 
                        get_nms(pred_bbox[i], pred_obj[i], max_output, iou_threshold, score_threshold, i), 
                        tf.cast(tf.range(batch_size), tf.int32)) #[batch_size, max_output, 2]
    return nms
  

