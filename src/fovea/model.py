import os
import tensorflow as tf
import numpy as np
from tqdm import trange
from IPython import display

from .net import FoveaNet
from .dataloader import DataLoader
from .loss import Loss
from .tf_utils import FoveaCoder, get_batch_nms
from utils import log as L
from fovea.box_utils import evaluate_mAP



flat = lambda x: tf.concat([tf.reshape(p, [-1, np.product(p.shape.as_list()[1:-1]), 
                                    p.shape.as_list()[-1]]) for p in x], 1) #[batch_size, ?, -1]

class ModelBase():
    def __init__(self, config, img_size, class_dict, strides, normalize_factors, scale_ranges, 
                 pos_shrink, neg_shrink, reset_graph= True, **net_args):
        self.config = config
        self.img_size = H, W= img_size
        self.n_class = len(class_dict)
        self.class_dict = class_dict

        if reset_graph:
            tf.reset_default_graph()
        self.graph = tf.Graph()
        with self.graph.as_default():
            with tf.name_scope('network'):
                self.img = tf.placeholder(tf.float32, [None, H, W, 3])
                self.is_training = tf.placeholder(tf.bool, [])
                self.net = FoveaNet(self.n_class, **net_args)
                self.pred =self.net.forward(self.img, self.is_training)
                self.feature_dims = self.net.conv_dim
                self.coder = FoveaCoder(self.net.conv_dim, strides, normalize_factors, self.n_class, scale_ranges)
                self.pred_bbox = self.coder.decode(self.pred)
                self.pred, self.pred_bbox = flat(self.pred), flat(self.pred_bbox)
            self.saver = tf.train.Saver()  

    def define_target(self):
        with self.graph.as_default():
            with tf.name_scope('target'):
                self.gt_box = tf.placeholder(tf.float32, [None, None, 5])
                self.target = self.coder.encode(self.gt_box)
                self.true_bbox = self.coder.decode(self.target)
                self.target, self.true_bbox = flat(self.target), flat(self.true_bbox)

    def save_model(self, sess, save_path):
        self.saver.save(sess, save_path + '/model.ckpt') 

    def restore_model(self, sess, restore_path):
        self.saver.restore(sess, restore_path + "/model.ckpt")  

    def define_dataset(self, filename, **kwargs):
        with self.graph.as_default():
            self.dat = DataLoader(filename, self.img_size, self.class_dict, **kwargs)

            
class ModelEval(ModelBase):
    def __init__(self, base_args, data_args, nms_args):
        ModelBase.__init__(self, **base_args)
        self.define_dataset(**data_args)
        self.define_nms(**nms_args)

    def define_nms(self, obj_threshold= 0.5, nms_threshold = 0.4,  nms_max_n = 100):
        with self.graph.as_default():
            self.pred_class = tf.sigmoid(self.pred[..., 4:])
            pred_obj = tf.reduce_max(self.pred_class, -1)
            self.prediction = tf.concat([self.pred_bbox, pred_obj[..., None], self.pred_class], -1)
            self.nms = get_batch_nms(self.pred_bbox, pred_obj, nms_max_n, nms_threshold, obj_threshold )
            
            
def poly_decay(init_lr, power, epoch, total_epoch):
    return init_lr * (1- epoch/total_epoch)**power
    
class ModelTrain(ModelBase):
    def __init__(self, base_args, data_args, opt_args):
        ModelBase.__init__(self, **base_args)
        self.define_dataset(**data_args)
        self.define_optimizer(**opt_args)

    def define_dataset(self, train_filename, valid_filename, **kwargs):
        with self.graph.as_default():
            self.train_dat = DataLoader(train_filename, self.img_size, self.class_dict, **kwargs)
            self.valid_dat = DataLoader(valid_filename, self.img_size, self.class_dict, **kwargs)

    def define_optimizer(self, init_lr, lr_decay, alpha_l2, alpha_bbox, alpha_class, momentum=0.9, **loss_args): 
        with self.graph.as_default():
            with tf.name_scope('loss'):
                self.define_target()
                loss_fn = Loss(**loss_args)
                self.loss_dict, self.nms = loss_fn(self.target, self.pred, self.true_bbox, self.pred_bbox)
                loss =  alpha_l2 * tf.reduce_mean([tf.nn.l2_loss(v) for v in tf.trainable_variables()])
                loss += alpha_class * self.loss_dict['loss_class']
                loss += alpha_bbox * self.loss_dict['loss_bbox']
                self.loss_dict['loss'] = loss

            with tf.name_scope('optimizer'):
                self.init_lr = init_lr
                self.lr_decay = lr_decay
                self.lr = tf.placeholder(tf.float32, [])
                opt = tf.train.MomentumOptimizer(self.lr, momentum)  
                v = tf.trainable_variables()
                g = tf.gradients(loss, v)
                #g = tf.clip_by_global_norm(g, 5)[0]
                opt = opt.apply_gradients(list(zip(g,v)))
                self.opt = tf.group([opt, tf.get_collection(tf.GraphKeys.UPDATE_OPS)])
        
    def one_epoch(self, sess, dat, epoch_size, lr, train=False, mAP = True):
        mAP_value = self.mAP
        summary =  np.zeros( len(self.loss_dict.keys()) )
        sess_op = [self.loss_dict]
        if train:
            sess_op += [self.opt]
        if mAP:
            sess_op += [self.nms]
            true_box_lists, pred_box_lists = [], []  

        for _ in trange(epoch_size):
            try: 
                img, gt_box = sess.run(dat.next_op)
            except:
                dat.initialize(sess) 
                img, gt_box = sess.run(dat.next_op)
            feed_dict = {self.img: img, self.gt_box: gt_box, self.lr: lr, self.is_training: True}
            sess_value = sess.run(sess_op, feed_dict)
            loss_dict = sess_value[0]
            summary += list(loss_dict.values())

            if mAP: #ignore labels
                pred_nms = sess_value[-1]
                pred_nms = np.concatenate((pred_nms, np.zeros_like(pred_nms[..., 0:2])), -1)
                true_box_list = gt_box[..., 0:5].copy()
                true_box_list = [[(b[0:4].tolist() + [0]) for b in box if b[4]>=0] for box in true_box_list]
                pred_box_list = [p[np.prod(p[..., 0:4], -1)>0] for p in pred_nms]
                true_box_lists += true_box_list
                pred_box_lists += pred_box_list
            
            
        summary /= epoch_size
        if mAP:
            self.mAP= evaluate_mAP(pred_box_lists, true_box_lists, 1, 0.4)
        return summary.tolist() + [self.mAP]           

    def train(self, epoch_n, save_epoch_n, train_epoch_size, valid_epoch_size, save_path, 
              restore_path = None, restore_pkl = None, show_fig_start = 0, mAP_epoch_n = 10, valid_ave_n = 5):
        os.makedirs(save_path, exist_ok = True)
        log = L.Log(list(self.loss_dict.keys()) +['mAP'] , save_path, valid_ave_n)
        best_loss = 0
        self.mAP = 0
        with tf.Session(graph = self.graph, config =self.config) as sess:
            init = [tf.global_variables_initializer(), tf.local_variables_initializer()]
            sess.run(init)
            self.train_dat.initialize(sess)
            self.valid_dat.initialize(sess)

            if restore_pkl is not None:
                self.net.backbone.load_pkl(sess, restore_pkl)
            if restore_path is not None:
                self.restore_model(sess, restore_path)
            self.graph.finalize() 

            for epoch in trange(epoch_n):
                lr_now = poly_decay(self.init_lr, self.lr_decay, epoch, epoch_n)
                train_summary = self.one_epoch(sess, self.train_dat, train_epoch_size, lr_now, True, 
                                              epoch %mAP_epoch_n==0)
                valid_summary = self.one_epoch(sess, self.valid_dat, valid_epoch_size, lr_now, False,
                                              epoch %mAP_epoch_n==0)

                for i, j in enumerate(log.var_names):
                    log.update_value(epoch, j, train_summary[i], valid_summary[i])
                log.update_log(epoch)
                display.clear_output(wait=True)
                log.update_plot(show_fig_start)

                if epoch % save_epoch_n==0:
                    self.save_model(sess, save_path)
                
                loss_now = -log.evaluation['ave_valid_recall'][epoch]
                if loss_now< best_loss:
                    best_loss = loss_now
                    self.save_model(sess, save_path + '/best')
