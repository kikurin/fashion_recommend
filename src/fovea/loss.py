import tensorflow as tf
from .tf_utils import get_batch_nms



class Loss():
    def __init__(self, focal_alpha = 0.25, focal_gamma=2, smooth_l1_beta=0.11, iou_threshold=0.5, 
                 obj_threshold=0.5, nms_threshold = 0.4, nms_max_n = 100):
        self.focal_loss = lambda logits, labels: focal_loss(logits, labels, focal_alpha, focal_gamma)
        self.smooth_loss = lambda x, y: smooth_l1_loss(x, y, smooth_l1_beta)
        self.iou_thresh = iou_threshold
        self.obj_thresh = obj_threshold
        self.nms_thresh = nms_threshold
        self.nms_max_n = nms_max_n


    def __call__(self, target, pred, true_bbox = None, pred_bbox = None):
        '''
        Args: 
            - target: [batch_size, ?,  4+n_class], encoded values
            - pred: [batch_size, ?, 4+n_class] output from FoveaNet
            - true_bbox, pred_bbox: [batch_size, ?, 4], original(decoded) bbox's coordinates
        Returns:
            - loss_dict 
        '''
        # loss_class
        labels, logits = target[..., 4:], pred[..., 4:]
        class_mask = tf.cast(labels>=0, tf.float32)
        loss_class = self.focal_loss(logits, labels)
        loss_class = tf.reduce_sum(loss_class * class_mask)/tf.reduce_sum(class_mask)
        
        # loss_offset 
        t_offset = target[..., 0:4]
        p_offset= pred[..., 0:4]
        bbox_mask = tf.reduce_max(tf.cast(tf.abs(labels)>0, tf.float32), -1) #[batch_size, ?]
        n_bbox = tf.reduce_sum(bbox_mask)
        loss_bbox = self.smooth_loss(t_offset, p_offset)
        loss_bbox = tf.reduce_sum(loss_bbox * bbox_mask[..., None])/n_bbox
        
        loss_dict =  {'loss_bbox': loss_bbox, 'loss_class': loss_class}
        nms = None
        
        # recall
        if true_bbox is not None:
            ious = ious_over_matrix(true_bbox, pred_bbox) *bbox_mask
            pred_obj = tf.sigmoid(tf.reduce_max(logits, -1))
            
            obj_mask = tf.cast(pred_obj *bbox_mask > self.obj_thresh, tf.float32)
            hit_bbox_mask = tf.cast(ious > self.iou_thresh, tf.float32) 
            recall_bbox = tf.reduce_sum(hit_bbox_mask)/n_bbox
            recall = tf.reduce_sum(hit_bbox_mask * obj_mask)/n_bbox
            loss_dict.update({'recall_bbox': recall_bbox, 'recall': recall})      

            nms = get_batch_nms(pred_bbox, pred_obj,  self.nms_max_n, self.nms_thresh, self.obj_thresh)  #[batch_size, max_output, 2]
            nms_mask = tf.where(nms[..., 1] >0, tf.ones_like(nms[..., 1]), tf.zeros_like(nms[..., 1]))[..., None] 
            pred_nms = tf.gather_nd(pred_bbox, nms) * tf.cast(nms_mask, tf.float32) #[batch_size, max_output, 4]            
        return loss_dict, pred_nms

                     
def focal_loss(logits, labels, alpha=0.25, gamma=2):
    '''
    Args:
        - logits, labels:  tf.float32, [...]
        - alpha: float between [0, 1], weights for negative smaples
            - if alpha<0.5, more weights on positive examples
        - gamma: the higher gamma is, the  lower weight for easy-classifying examples. 
    Returns: 
        -loss: tf.float, [...]
    '''
    p = tf.nn.sigmoid(logits)
    pt =  tf.where(tf.equal(labels, 1), p, 1-p)
    alpha_t = tf.where(tf.equal(labels, 1), (1-alpha) * tf.ones_like(labels), (alpha)*tf.ones_like(labels))
    loss = -(1-pt)**gamma * alpha_t * tf.log(pt)
    return loss

def smooth_l1_loss(x, y, beta = 1.0):
    loss = tf.abs(x -y)
    loss = tf.where(loss<beta, 0.5*loss**2/beta, loss - 0.5 *beta)
    return loss    

def ious_over_matrix(bbox1, bbox2):     
    # bbox1, bbox2: tf.float32, [...., 4(xmin, ymin, xmax, ymax)]
    # ious: [....]
    min1, max1 = bbox1[..., 0:2], bbox1[..., 2:4]
    min2, max2 = bbox2[..., 0:2], bbox2[..., 2:4]
    wh1 = max1- min1
    wh2 = max2 - min2

    inter_min = tf.maximum(min1, min2)
    inter_max = tf.minimum(max1, max2)
    inter_wh = inter_max - inter_min
    inter_area = inter_wh[..., 0] * inter_wh[..., 1]
    area1 = wh1[..., 0] * wh1[..., 1]
    area2 = wh2[..., 0] * wh2[..., 1]
    ious = inter_area/(area1+area2 - inter_area +1e-6) 
    return ious