import tensorflow as tf
import tensorflow.contrib.slim as slim
from .backbone_net import YoloV3, Resnet


class FoveaNet():
    def __init__(self, n_class, n_feature=256, network = 'Resnet', **backbone_args):
        if network in ('Resnet', 'resnet'):
            self.backbone = Resnet(scope='backbone', **backbone_args)
        else:
            self.backbone = YoloV3(scope='backbone', **backbone_args)
        self.fpn = FPN(n_feature, n_downsample_layer = 2, scope='FPN')
        self.predhead = PredictHead(n_class, n_feature)

    def forward(self, x, is_training):
        out = self.backbone.forward(x, is_training)
        fpn_out = self.fpn.forward(out[-3:])
        self.conv_dim = [c.shape.as_list()[1:3] for c in fpn_out]
        prediction = self.predhead.forward(fpn_out) #[?, conv_h, conv_w, 4 + n_class]
        return prediction


class FPN():
    def __init__(self, n_feature = 256, n_downsample_layer = 2, scope='FPN'):
        self.scope = scope
        self.n_feature =  n_feature
        self.n_downsample_layer = n_downsample_layer

    def forward(self, fpn_input):
        # fpn_input, out: list of [B, H, W, C] tensors, from higher to lower resolution
        fpn_input = fpn_input[::-1] # low --> high
        with tf.variable_scope(self.scope):
            out, x = [], 0
            for i, down in enumerate(fpn_input):
                if i>0:
                    size = down.shape.as_list()[1:3]
                    x = tf.image.resize_bilinear(x, size)
                x += slim.conv2d(down, self.n_feature, [1, 1], scope='lateral_%d'%i, activation_fn=None)
                out.append(x)

            for i, x in enumerate(out):
                out[i] = slim.conv2d(x, self.n_feature, [3, 3], scope='predict_%d'%i, activation_fn = tf.nn.relu)

            x = out[0] # lowest reso so far
            out = out[::-1] # high --> low
            for i in range(self.n_downsample_layer):
                x = slim.conv2d(x, self.n_feature, [3, 3], stride = 2, scope='downsample_%d'%i, 
                                activation_fn=None)
                out.append(x) 
            return out         



class PredictHead():
    def __init__(self, n_class, n_feature = 256, scope='Prediction'):
        self.n_class = n_class
        self.n_feature = n_feature
        self.scope =scope

    def forward(self, feature_list):
        # feature_list: lists of m features map([B, H, W, C]) from higher to lower resolution (output from FPN) 
        c = self.n_class
        pred = []
        for i, x in enumerate(feature_list):
            with tf.variable_scope(self.scope, reuse=tf.AUTO_REUSE):
                x_bbox  = slim.repeat(x, 4, slim.conv2d, self.n_feature, [3, 3],  scope='conv_feature_bbox', activation_fn=tf.nn.relu)    
                x_logit= slim.repeat(x, 4, slim.conv2d, self.n_feature, [3, 3],  scope='conv_feature_logit', activation_fn=tf.nn.relu)    
                x_bbox = slim.conv2d(x_bbox, 4, [3, 3], scope='conv_bbox', activation_fn = None) 
                x_logit = slim.conv2d(x_logit, c, [3, 3], scope='conv_logit', activation_fn = None)
                pred.append(tf.concat([x_bbox, x_logit], -1))
                
        return  pred
